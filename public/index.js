import * as FingerprintJS from './lib/externals/fingerjs.js';
import BigMath from './lib/BigMath.js';


let terranium;

const color = Chart.helpers.color;
const charts = {};
//const rootpath = 'https://mnb-web-on-cloudflare.pages.dev';
let rootpath = window.location.href;
while(rootpath.endsWith('/')){
	rootpath = rootpath.split('').slice(0,-1).join('');
}

const fingerprint = FingerprintJS.load();


async function lookupScatter() {
	let nodes = `${rootpath}/scores`;
	nodes = await fetch(nodes);
	nodes = await nodes.json();
	nodes = Object.values(nodes).map((d)=>{
		d.gen = d.generation;
		delete d.generation;
		return d;
	});
	UpdateBees(nodes);
	return nodes;
}

function InitScatter(){
	let ctx = document.getElementById('canvas').getContext('2d');
	charts.bees = Chart.Scatter(ctx, {
		type:"line",
		data: {
			datasets: [{
				label: 'Scores',
				borderColor: color('darkgray').rgbString(),
				pointStyle: 'circle',
				pointRadius: 5,
				pointHoverRadius: 5,
				data: [],
				pointBackgroundColor: function(context) {
					let i = context.dataIndex;
					let shade = 0;
					let value = context.dataset.data[i];
					if(value){
						context.dataset.maxOva = Math.max(value.ova,context.dataset.maxOva??0);
						shade = (value.ova/context.dataset.maxOva);
						shade *= 11/12;
						shade += 1/12;
					}
					return color('gray').alpha(shade).rgbString();
                }
			}]
		},
		options: {
			legend:{
				display:false,
			},
			title: {
				display: false,
				text: 'MNB Learning'
			},
			tooltips: {
				enabled: false
			},
			scales: {
				xAxes: {
					ticks: {
						precision: 0
					}
				}
			},
			scales: {
				yAxes: [{
					ticks: {
						precision: 0
					}
				}]
			},
		}
	});
}

async function lookupBar(){
	//let nodes = 'http://127.0.0.1:5984/sec-mnb/_design/score/_view/score?reduce=true&group=true&group_level=4&descending=true&limit=100';
	let nodes = `${rootpath}/scores?reduce=true`;
	nodes = await fetch(nodes);
	nodes = await nodes.json();
	let scores = Object.values(nodes)
		.reduce((a,d)=>{
			let gen = Math.floor(d.generation / 100);
			a[gen] = a[gen] ?? [];
			a[gen].push(d.score);
			return a;
		},{});
	let stats = {};
	for(let i in scores){
		let s = {
			gen: i,
			sum: scores[i].reduce((a,d)=>a+d),
			count: scores[i].length,
			min: Math.min(...scores[i]),
			max: Math.max(...scores[i]),
		}
		s.mean = s.sum / s.count;
		s.dev = scores[i].map(d=>(d-s.mean)**2).reduce((a,d)=>a+d);
		s.dev /= s.count;
		s.dev = s.dev**0.5;
		s.neg = s.mean-s.dev*3;
		s.mid = s.mean;
		s.pos = s.mean+s.dev*3;
		stats[i] = s;
	}

	stats = Object.values(stats);
	return stats;
}

function InitBar(){
	let ctx = document.getElementById('bars').getContext('2d');
	charts.bars = new Chart(ctx, {
		type: 'bar',
		data: {
			datasets:[
				{
					xAxisID: "bar-x-axis2",
					backgroundColor: color('white').alpha(1).rgbString(),
					borderColor: color('grey').alpha(0.5).rgbString(),
					borderWidth:6,
					barPercentage: 0.9,
					categoryPercentage: 0.8,
					data: [],
				},
				{
					xAxisID: "bar-x-axis1",
					backgroundColor: color('grey').alpha(0.5).rgbString(),
					borderColor: color('grey').alpha(0.5).rgbString(),
					borderWidth:0,
					data: [],
				},
				{
					xAxisID: "bar-x-axis3",
					backgroundColor: color('white').alpha(0).rgbString(),
					borderColor: color('grey').alpha(0.5).rgbString(),
					borderWidth:0,
					barPercentage: 0.9,
					categoryPercentage: 0.8,
					data: [],
				},
			]
		},
		options: {
			legend:{
				display:false,
			},
			plugins:{
				title: {
					text: 'MNB Learning'
				},
			},
			tooltips: {
				enabled: false
		   	},
			scales: {
				xAxes: [
					{
						stacked: true,
						id: "bar-x-axis1",
					},
					{
						display: false,
						stacked: true,
						id: "bar-x-axis2",
						type: 'category',
						gridLines: {
							offsetGridLines: true
						},
						offset: true
					},
					{
						display: false,
						stacked: true,
						id: "bar-x-axis3",
						type: 'category',
						gridLines: {
							offsetGridLines: true
						},
						offset: true
					},
				],
				yAxes: [{
					stacked: false,
					ticks: {
						beginAtZero: true
					},
				}]

			}
		}
	});
}

function UpdateBees(target){
	charts.bees.data.datasets[0].maxOva = charts.bees.data.datasets[0].data.reduce((a,d)=>Math.max(a,d.ova),0);
	let data = charts.bees.data.datasets[0].data;
	data = data.filter(d=>d);
	charts.bees.data.datasets[0].data = data;
	target = target.reduce((a,d)=>{
		a[d.id] = d;
		return a;
	},{});
	let map = charts.bees.data.datasets[0].map||{};
	charts.bees.data.datasets[0].map = map;
	let adds = Object.keys(target).filter((t)=>{
		return !(t in map);
	});
	let rems = Object.keys(map).filter((m)=>{
		return !(m in target);
	});
	for(let a of adds){
		a = target[a];
		map[a.id] = a;

		a.x = a.gen;
		a.y = a.score;
		a.targ = { x:a.x, y:a.y ,v:0};

		a.vel = Number.parseInt(a.id,32);
		a.vel = {x:a.vel/0xffff/0xffff, y:a.vel%0xffff/0xffff};
		a.vel = {x:a.vel.x*2-1, y:a.vel.y*2-1};
		//a.vel = {x:a.vel/0xffff, y:a.vel%0xffff};
		//let tot = a.vel.x + a.vel.y;
		//a.vel = {x:(a.vel.x/tot)*2-1, y:(a.vel.y/tot)*2-1};

		data.push(a);
	}
	for(let a of rems){
		let i = 0;
		for(i=0; i<data.length; i++) if(data[i] && a === data[i].id) break;
		map[a].v = 0;

		delete data[i];
		delete map[a];
	}
	charts.bees.data.datasets[0].maxOva = charts.bees.data.datasets[0].data.reduce((a,d)=>Math.max(a,d.ova),0);
	charts.bees.stasisAt = Date.now() + 10000;
	charts.bees.update();
	setTimeout(TickBees,300);
}

function Distance(a,b,applyRadius=false){
	let dx = a.x-b.x;
	let dy = a.y-b.y;
	let len = Math.pow(dx*dx+dy*dy, 0.5);
	len = Math.abs(len);
	if(applyRadius === true){
		len -= a.v||0;
		len -= b.v||0;
	}
	return len;
}


let TickBeesTicker = null;
function TickBees(){
	clearTimeout(TickBeesTicker);

	let data = charts.bees.data.datasets[0].data;
	let map  = charts.bees.data.datasets[0].map;

	let stasis = true;

	//let travelTime = 1000;
	let tickTime = 35;
	//let travelPortion = tickTime/travelTime;
	//let a = 1 / (travelTime**2) / travelPortion;

	// collision detection
	let pScale = {
		x: charts.bees.scales['x-axis-1'],
		y: charts.bees.scales['y-axis-1'],
	}
	pScale = {
		x: (pScale.x._endValue-pScale.x._startValue)/(pScale.x._endPixel-pScale.x._startPixel),
		y: (pScale.y._endValue-pScale.y._startValue)/(pScale.y._endPixel-pScale.y._startPixel),
	};
	pScale.p = Distance(pScale,{x:0,y:0});
	pScale.r = charts.bees.data.datasets[0].pointRadius,
	pScale.d = charts.bees.data.datasets[0].pointRadius*2,
	pScale.dx = pScale.d*pScale.x;
	pScale.dy = pScale.d*pScale.y;
	pScale.dxy = ((pScale.dx**2+pScale.dy**2))**0.5;


	let minX = Number.MAX_SAFE_INTEGER;
	let maxX = Number.MIN_SAFE_INTEGER;
	let collisions = data.slice().map(d=>{
		d = JSON.parse(JSON.stringify(d));
		d.v *= pScale.p;
		return d;
	});
	for(let i=collisions.length-1; i>=0; i--){
		let a = collisions[i];
		if (!a) continue;

		minX = Math.floor(Math.min(minX, a.targ.x));
		maxX = Math.ceil(Math.max(maxX, a.targ.x));
		for(let d of ['y','x']){
			let dist = a.targ[d] - a[d];
			let amt = a.vel[d] * pScale[d];
			amt = Math.abs(amt);
			amt = Math.min(amt, pScale[d]*0.05);
			amt = Math.min(amt, Math.abs(dist));
			amt *= Math.sign(dist);
			data[i][d] += amt;
			data[i][d] = Math.max(0, data[i][d]);
		}

		let didCollide = false;
		for(let j=i-1; j>=0; j--){
			let b = collisions[j];
			if (!b) continue;
			let dx = (a.x-b.x)/pScale.x;
			let dy = (a.y-b.y)/pScale.y;
			let dist = (dx**2+dy**2) ** 0.5;
			if(dist < pScale.d){
				didCollide = true;
				// only ever move one. Pick the one with the lessor ID
				let ab = (a.id < b.id) ? [i,j] : [j,i];

				// backward or forward... move backward more slowly
				// never do this. It is much more complicated to determine if it is too
				// far from teh group, than it is to determine if it is overlapping any
				// single individual. This starts getting into "which group?", and "check
				// the gap distance to everyone". It get's messy.
				let dir = dist < pScale.d ? 0.5 : -0.005;
				for(let xy of ['x','y']){
					let amt = 0;
					amt += Math.min(pScale[xy], data[ab[0]].vel[xy] * pScale[xy]);
					amt -= Math.min(pScale[xy], data[ab[1]].vel[xy] * pScale[xy]);
					//amt += amt * Math.random() * 0.01
					amt /= 4;
					if(amt !== 0){
						stasis = false;
					}
					for(let d of ab){
						data[d][xy] += amt * dir;
						if(data[d][xy]<0){
							data[d][xy] = 0;
							data[d].vel[xy] = Math.abs(data[d].vel[xy]);
						}
						amt *= -1;
					}
				}
			}
		}
		/*
		if(!didCollide){
			let datum = data[i];
			let dist = {
				x: datum.targ.x-datum.x,
				y: datum.targ.y-datum.y,
			};
			if(pScale.x > Math.abs(dist.x) && pScale.y > Math.abs(dist.y)){
				datum.x = datum.targ.x;
				datum.y = datum.targ.y;
			}
			else{
				for(let d of ['y','x']){
					let amt = datum.vel[d] * pScale[d];
					amt = Math.abs(amt);
					amt = Math.min(amt, pScale[d]*0.85);
					amt += pScale[d] * Math.random() * 0.15;
					//amt = Math.min(amt, Math.abs(dist[d]));
					amt *= Math.sign(dist[d]);
					datum[d] += amt;
					datum[d] = Math.max(0, datum[d]);
				}
			}
		}
		*/
	}

	charts.bees.options.scales.xAxes[0].ticks.min = minX-1;
	charts.bees.options.scales.xAxes[0].ticks.max = maxX+1;
	charts.bees.update();

	if(stasis || charts.bees.stasisAt < Date.now()){
		tickTime *= 1000;
	}
	clearTimeout(TickBeesTicker);
	TickBeesTicker = setTimeout(TickBees,tickTime);
}


class basicNumbers{
	#data = {};

	constructor(selector='#text',apiroot='https://edgar.vius.workers.dev/'){
		this.context = document.querySelector(selector);
		this.api = apiroot;
		this.#data = {
			'size': {},
			'gufsize':{},
			'score':{
				'avg':{},
				'min':{},
				'max':{},
			},
			'gen':{
				'avg':{},
				'min':{},
				'max':{},
			}
		}
	}

	async fetch(){
		let nodes = null;
		nodes = `${this.api}/scores`;
		nodes = await fetch(nodes);
		nodes = await nodes.json();
		nodes = Object.values(nodes).map(d=>d.score);
		this.data.size = nodes.length;
		this.data.score.avg = nodes.reduce((a,d)=>a+d,0)/this.data.size;
		this.data.score.min = nodes.reduce((a,d)=>Math.min(a,d),Number.MAX_SAFE_INTEGER);
		this.data.score.max = nodes.reduce((a,d)=>Math.max(a,d),Number.MIN_SAFE_INTEGER);

		/*
		nodes = this.api + '_design/guf/_view/guf?reduce=true';
		nodes = await fetch(nodes);
		nodes = await nodes.json();
		nodes = nodes.rows.pop();
		this.data.gufsize = nodes.value;
		*/
		this.update();
	}

	get data(){
		return this.#data;
	}

	update(){

		function apply(data, context){
			for(let d in data){
				let dat = data[d];
				let elem = `[name="${d}"]`;
				elem = context.querySelector(elem);
				if(typeof dat === 'object'){
					apply(dat,elem);
				}
				else{
					elem.value = (Math.floor(dat*100)/100);
				}
			}
		}

		apply(this.data,this.context);

	}
}

function UpdateMultipleBugs(lives){
	let prog  = document.querySelector('#beeProgress > table > tbody');
	for(let life of lives){
		let tr = prog.querySelector(`tr[data-id="${life.id}"]`);
		if(!tr){
			tr = document.createElement('tr');
			tr.dataset.id = life.id;
			prog.prepend(tr);
		}

		const maxAge = 65534;
		let pct = (life.age/maxAge).toFixed(2);

		let lookupDate = life.sensors.date;
		lookupDate = lookupDate.toISOString().split('T')[0];

		//let sign = '🡇 🡅'.charAt(Math.sign(life.actuators.predChg)+1);
		let sign = '-±+'.charAt(Number(BigMath.sign(life.actuators.predChg))+1);
		let predChange = BigMath.abs(life.actuators.predChg);
		predChange = Number(BigMath.min(predChange,99.9)).toFixed(1);

		let predStart = new Date();
		predStart.setDate(predStart.getDate()+Number(life.actuators.predStart));
		let predFin = new Date(predStart.getTime());
		predFin.setDate(predFin.getDate()+Number(life.actuators.predFin));
		[predStart,predFin] = [predStart,predFin].map(d=>d.toISOString().split('T')[0]);

		tr.innerHTML = [
			`<td title='${Math.floor(pct*100)}%'><progress style='min-width:3em;' class='${life.age>=maxAge?'done':''}' min='0' max='${maxAge}' value='${life.age}'/></td>`,
			`<td>${life.id.split('.').pop()}</td>`,
			`<td>${life.sensors.comp}<br />@${lookupDate}</td>`,
			`<td>${sign}${predChange}%</td>`,
			`<td>${predStart} / ${predFin}</td>`,
			`<td>${sign}${predChange}%</td>`,
			`<td>Score</td>`,
		].join('\n');

		if(life.age >= maxAge){
			tr.style.opacity = '1';
			tr.style.transition = 'opacity 5s';
			setTimeout(()=>{
				tr.style.opacity = '0';
				setTimeout(()=>{tr.parentElement.removeChild(tr);},5000);
			},1800000);
		}

	}
}


window.addEventListener('load',async function() {
	let stats = new basicNumbers('#text',rootpath);
	let prog  = document.querySelector('#UniverseLifespan');
	InitScatter();
	InitBar();
	//InitBees();

	let fp = (await (await fingerprint).get()).visitorId;
	terranium = new Worker(`./lib/mnb/app/app-worker.js?fingerprint=${fp}`, {type:"module"});
	terranium.onmessage = (e)=>{
		let data = e.data.data;
		let type = e.data.type;
		switch(type){
			case 'progress':
				prog.classList.remove('done');
				prog.max = data.total ?? 0;
				prog.value = Math.max(data.remaining ?? 0);
				UpdateMultipleBugs(data.lifes);
				break
			case 'bugdone':
				// a specific bug was completed
				break;
			case 'UniverseTerminate':
				prog.classList.add('done');
				prog.value = 1;
				prog.max = 1;
				setTimeout(lookupScatter,500);
				break;
			}
	};

	let timer = null;
	async function update(){
		stats.fetch();

		let nodes = await lookupScatter();
		UpdateBees(nodes);
		nodes = await lookupBar();
		charts.bars.data.labels = nodes.map(d=>d.gen);
		for(let n of nodes){
			let midOffset = Math.ceil(Math.abs(n.pos-n.neg)/n.pos) * 2;
			for(let i=charts.bars.data.datasets.length-1; i>=0; i--){
				charts.bars.data.datasets[i].data.borderWidth = midOffset;
			}
			charts.bars.data.datasets[0].data.push([n.mid-midOffset,n.mid+midOffset]);
			charts.bars.data.datasets[1].data.push([n.neg,n.pos]);
			charts.bars.data.datasets[2].data.push([n.min,n.max]);
		}
		charts.bars.update();

		clearTimeout(timer);
		timer = setTimeout(update,1356711+(Math.random()*120000));
	}
	update();
});


