
const ONEDAY = 24 * 60 * 60 * 1000;


export {
	wait,
	utcParse,
	sampler,
	DateToDay,
	DayToDate,
	shuffle,
};

let wait = function wait(time=0){
	time = Math.max(time,16);
	return new Promise((resolve)=>{
		setTimeout(resolve,time);
	});
};


let utcParse = function utcParse(date){
	date = date.replace(/[^0-9]/g,'-');
	date = date.split('-');
	date = date.map(d=>{return +d;});
	date[1] = date[1] - 1;
	date = Date.UTC(... date);
	date = new Date(date);
	return date;
};

let sampler = function sampler(value,probability=1.1){
	let dispose = hasher(value, 32) / 32;
	let iskeeper = (dispose < probability);
	return iskeeper;
}


function DateToDay(date){
	if (typeof date === 'bigint'){
		date = Number(date);
	}
	if (typeof date === 'number'){
		date = new Date(date);
	}
	if(date instanceof Date){
		date = date.toISOString();
	}
	date = date.split('T')[0];
	date = date.split('-');
	date = Date.UTC(... date);
	date = Math.round(date / ONEDAY);
	return date;
}



function DayToDate(days){
	if(typeof days === 'bigint'){
		days = Number(days);
	}
	days = Math.min(days, 100000000);
	days = Math.round(days);
	
	let date = new Date(0);
	date.setDate(date.getDate()+days);
	return date;
}


function hasher(buffer,bits=32){
	if(typeof buffer === 'string'){
		buffer = buffer
			.split('')
			.map(d=>{return d.charCodeAt(0);})
			;
	}
	if(typeof buffer[Symbol.iterator] !== 'function'){
		console.debug(`buffer: ${buffer}`);
	}
	let size = Math.pow(2,bits);
	let hash = 0;
	for(let d of buffer){
		hash *= 2;
		hash += Math.floor(hash/size);
		hash %= size;
		hash ^= d;
	}
	return hash;
};

function shuffle(list){
	for(let i = list.length-1; i > 0; i--){
		let j = Math.round(Math.random() * i);
		let swap = list[i];
		list[i] = list[j];
		list[j] = swap;
	}
}
