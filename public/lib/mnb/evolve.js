import Bug from './app/animatSecOneComp.js';
import Company from '../library/company.js';
import * as utils from '../utils.js';

export default class Evolve extends EventTarget{

	constructor(db){
		super();
		this.db = db;
	}

	async seedGuf(bug) {
		let parentKeys = JSON.stringify(Array.from(bug.genome.parents).map(d=>d.toString(32)));
		let key = bug.genome.id.toString(32);
		console.log(`Seeding genome ${key} with parents ${parentKeys}`);

		// save it to the guff
		await this.db.saveBug(bug);
	}

	async pullFromGuf(){
		let resp = `${this.db.db}/evolve/breeder`;
		resp = await fetch(resp);

		let blob = await resp.blob();
		blob = await blob.arrayBuffer();
		let bug = new Bug(blob);

		let parentKeys = JSON.stringify(Array.from(bug.genome.parents).map(d=>d.toString(32)));
		let key = bug.genome.id.toString(32);
		console.log(`Pulling genome ${key} with parents ${parentKeys} `
			+ Array.from(bug.genome.slice(0,12)).map(d=>{return d.toString(16).padStart(8,'0');})
			.join('-'));

		return bug;
	}

	async definePurpose(goal=null){
		
		if(!goal){
			let resp = `${this.db.db}/facts/goals`;
			resp = await fetch(resp);
			resp = await resp.json();

			goal = [resp.comp, utils.DayToDate(resp.date), resp.days];
		}

		goal = goal.slice(0,2);
		let comp = new Company(...goal);
		await comp.waitLoad;
		/*
		let blob = new Blob(comp.body);
		blob = await blob.arrayBuffer()
		blob = new Int32Array(blob);
		*/

		return comp;
	}
}
