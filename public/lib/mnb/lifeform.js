import Genome from './brain/genome.js';
import {Brain,BrainIO} from './brain/brain.js';

import GateTypes from './brain/gates.js';
import GateStats from './brain/gatesStats.js';
import * as utils from './utils.js';

export {
	LifeForm as default,
	LifeForm,
	BrainIO
};


class LifeForm extends EventTarget {
	static REGISTRY = new Map();
	static MAX_REWARD = 0xffffffff;
	//static MAX_AGE = 0xfffffffe;
	static MAX_AGE = 0xfffe;
	//static MAX_AGE = 4;
	static gatetypes = GateTypes.union(new Set(GateStats));

	constructor(genome=null,params={}) {
		super();
		this.gatetypes = LifeForm.gatetypes;
		this.genome = new Genome(genome);
		this.experience = [];
		this.parents = [];
		this.init(params);
		this.type = 'LifeForm';
	}

	toObject(){
		let rtn = {
			type: this.Type,
			id: this.id,
			age: this.age,
			genome: this.genome.toString(),
			experience:this.experience,
		};
		return rtn;
	}

	static fromObject(obj){
		let type = obj.type;
		if(!LifeForm.REGISTRY.has(type)){
			throw new Error('Unidentified Type of Object: ' + type);
		}
		type = LifeForm.REGISTRY.get(type);

		let rtn = new type(obj.genome);
		rtn.brain = Brain.fromObject(obj.brain, LifeForm.gatetypes);
		
		rtn.experience = obj.experience;
		rtn.init();
		rtn.age = obj.age;

		return rtn;
	}

	emit(event,detail=this){
		if(!(event instanceof Event)){
			event = new CustomEvent(event,{detail:detail});
		}
		this.dispatchEvent(event);
	}

	get id(){
		let id = [
			//utils.HashArray(fingerprint.split('').map(d=>{return Number.parseInt(d,16)})),
			utils.HashArray(this.Type.split('').map(d=>{return d.charCodeAt(0)})),
			this.genome.id,
		].map(d=>{
			d = d.toString(32).padStart(7,'0');
			return d;
		}).join('.');
		return id;
	}

	get Type(){
		return this.constructor.name;
	}


	init(params={}){
		// Rebuild the animal's brain
		if(!this.brain){
			this.genome.pos = 0;
			this.brain = new Brain(this.genome,this.gatetypes,params);
		}
		this.age = 0;

		this.actuators = new BrainIO(this.brain,0,1);
		this.sensors = new BrainIO(this.brain,this.actuators.length,1);
		this.actuators.reset(0);
	}

	get MIN_REWARD(){
		return -LifeForm.MAX_REWARD;
	}
	get MAX_REWARD(){
		return LifeForm.MAX_REWARD;
	}

	get MAX_AGE(){
		return LifeForm.MAX_AGE;
	}

	get reward(){
		let value = this.experience.positive - this.experience.negative;
		return value;
	}
	set reward(amt){
		amt = Math.floor(amt);
		if(amt === this.reward) return;
		this.emitChange('reward',this.reward,amt);
		amt -= this.reward;
		if(amt < 0){
			this.experience.negative -= amt;
			this.experience.negative = Math.min(this.MAX_REWARD, this.experience.negative);
			this.experience.negative = Math.floor(this.experience.negative);
		}
		else{
			this.experience.positive += amt;
			this.experience.positive = Math.min(this.MAX_REWARD, this.experience.positive);
			this.experience.positive = Math.floor(this.experience.positive);
		}

	}

	#isDone = false;
	get isDone(){
		if (this.#isDone) return true;
		if(this.age >= this.MAX_AGE){
			this.#isDone = true;
			setTimeout(()=>{this.emit('complete');});
			return true;
		}
		return false;
	}

	clone() {
		let genome = this.genome.clone();
		let bug = new LifeForm(genome);
		return bug;
	}

	measure(){
		if(this.isDone){
			return;
		}
		this.emit('measure');
	}
	think(){
		if(this.isDone){
			return;
		}
		this.brain.think(this.sensors);
		this.emit('think');
	}
	act(){
		if(this.isDone){
			return;
		}
		this.experience.push(Object.assign(this.actuators));
		this.age++;
		this.emit('act');
	}

}



