import 'https://cdnjs.cloudflare.com/ajax/libs/pouchdb/8.0.1/pouchdb.min.js';


export default class mnbDatabase extends EventTarget{

	constructor(db, opts = {}){
		super();
		if(db instanceof PouchDB){
			this.db = db;
		}
		else if(typeof db === 'string'){
			this.db = new PouchDB(db);
		}
		else{
			this.db = new PouchDB('mnbDatabase');
		}

		this.opts = {
			extinction: 0.1,
			maxItems: 1000,
			pauseWait: 10000,
		};
		this.opts = Object.assign(this.opts, opts);

		this.opts.extinction = Math.abs(this.opts.extinction);
		if(this.opts.extinction < 1){
			this.opts.extinction = this.opts.extinction * this.opts.maxItems;
		}
		this.opts.extinction = Math.max(10,this.opts.extinction);

		this.isPausedFlag = false,
		this.countDownToExtinction = 0;

		this._dbsize = null;
		setInterval(()=>{
			this._dbsize = null
		},600000);

	}

	get isPaused(){
		return !!isPausedFlag;
	}

	async WaitPaused(){
		while(isPaused()){
			await utils.wait(10000);
		}
	}

	destroy(){
		db.destroy();
	}


	async getBug(gufPos){
		let gen = utils.Pauser(()=>{return db.query('guf',{
			skip: gufPos,
			limit: 1,
			descending: true,
			attachments: true,
			reduce: false,
			include_docs:true,
		})});
		return gen;
	}

	async saveBug(bug){
		let resp = {
			done: bug.actuators.done,
			predict: bug.actuators.predict,
			predChg: bug.actuators.predChg,
			predStart: bug.actuators.predStart,
			predFin: bug.actuators.predFin,
		};
		let blob = bug.genome.blob;

		let rec = {
			type: 'animat',
			genome: {
				parents: Array.from(bug.genome.meta.parents.values()),
				karma: bug.genome.meta.karma,
				generation: bug.genome.meta.generation
			},
			bug: {
				id: bug.id,
				req: bug.sensorConstants,
				resp: resp,
				age: bug.age,
			},
			asat: Date.now(),
			_attachments:{
				genome:{
					content_type: 'application/octet',
					data: blob
				}
			}
		};

		try{
			await db.guf.post(rec);
			await cleanGuf();
		}
		catch(e){
			console.debug(e);
		}
	}


	/**************
	 * CLEAN THE GUFF
	 *
	 * At some point, we need to get rid of items
	 * that are not going to do anything. This is
	 * purely to conserve space.
	 */
	async cleanGuf(){
		/**************
		 * The extinction counter allows us to do
		 * clean up as a bulk operation. Rather than
		 * cleaning up on each pass, we clean up
		 * infrequently to allow it to save effort.
		 *
		 * We clean up 200 items, every 100 items.
		 * This way we clean up more mess than we
		 * make.
		 */
		this.countDownToExtinction--;
		if(this.countDownToExtinction>0){
			return;
		}
		this.countDownToExtinction = this.opts.extinction;

		/**************
		 * STERILIZE THE BOTTOM
		 *
		 * The first step is to sterilize the worst
		 * of our genomes. Sorted by score, take the
		 * worst and sterilize them.
		 **************/
		let recs = await utils.Pauser(()=>{ return db.guf.query('guf/guf',{
			skip:maxItems,
			limit:this.extinction*2,
			reduce:false,
			include_docs:true,
			descending:true
		})});
		let rows = [];
		for(let r of recs.rows){
			//r.doc.ova = 0;
			r = createDeleteDocument(r)
			rows.push(r.doc);
		}
		if (rows.length > 0) {
			await utils.Pauser(()=>{ return db.guf.bulkDocs(rows); });
		}

		/**************
		 * REMOVE USELESS GENOMES
		 *
		 * Genomes without ova are pointless. Delete the
		 * genomes if ova have gone to zero
		 **************/
		recs = await utils.Pauser(()=>{ return db.query('guf/uselessGenomes',{
			limit:this.extinction*2,
			reduce:false,
			include_docs:true
		})});
		rows = [];
		for(let r of recs.rows){
			r = createDeleteDocument(r)
			rows.push(r);
		}
		if (rows.length > 0){
			await utils.Pauser(()=>{ return db.bulkDocs(rows); });
		}


		/**********
		 * CLEAN OLD GENOMES
		 *
		 * In the event a lot of items have built up,
		 * just remove a bunch. We define this as 10x
		 * the number of buffered items, and remove a
		 * significant number of items.
		 */
		/*
		recs = await utils.Pauser(()=>{ return db.guf.query('guf/guf',{
			skip:maxItems,
			limit:100,
			reduce:false,
			descending: false,
			include_docs:true
		})});
		rows = [];
		for(let r of recs.rows){
			r = createDeleteDocument(r);
			rows.push(r);
		}
		await utils.Pauser(()=>{ return db.guf.bulkDocs(rows); });
		*/

		/**********************
		 * As we have deleted stuff, we should
		 * compact the database
		 *
		 * Some providers throw an error when issuing
		 * a `compact` because they don't support the
		 * operation. In this case, catch the error
		 * and turn off future attempts.
		 */
		let info = await db.guf.info();
		if(info.doc_count/2 < info.doc_del_count){
			console.log('Pack..: ');
			try{
				isPausedFlag = true;
				await db.guf.compact();
			}
			catch(e){
				debugger;
				console.error(e);
			}
			finally{
				isPausedFlag = false;
			}
		}
	}



	async getDbSize(){
		if(this._dbsize !== null) return this._dbsize;

		this._dbsize = new Promise(async (resolve)=>{
			async function tryit(){
				try{
					let size = await this.db.facts.info();
					resolve(size.doc_count);
				}
				catch(e){
					if(e.code !== 'ECONNRESET'){
						let size = {doc_count:0};
						resolve(size.doc_count);
					}
					setTimeout(tryit,16);
				}
			}
			tryit();
		});

		return _dbsize;
	}


}


function createDeleteDocument(r){
	r = r.doc;
	//for(let f in r){
	//	if(f.startsWith('_')) continue;
	//	delete r[f];
	//}
	r._deleted = true;
	r.asat = Date.now();
	return r;
}
