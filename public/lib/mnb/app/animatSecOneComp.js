import {LifeForm,BrainIO} from '../lifeform.js';
import * as utils from '../../utils.js';
import BigMath from '../../BigMath.js';

// The original plan was to keep each brain at about 1MB with the company 
// data being kept separately in some shared space. Periodically, a select 
// piece of company data (one row's worth) will be coppied into memory. 
// `comp_size` is the size of a row of data so that we can set aside (at 
// least) that much memory. 
const default_params = {comp_size:2**15}; // 32,768
default_params.min_brainSize = default_params.comp_size + 1024; // 33,792
default_params.max_brainSize = default_params.min_brainSize + (1024**2); // 1,048,576

export default class SecAnimatOneComp extends LifeForm{

	constructor(genome=null) {
		super(genome,default_params);

		if(!this.genome.parents.size){
			throw new Error('Faulty Genome. Parents not present, and are required for '+ this.constructor.name);
		}

		this.type = this.constructor.name;
		//this.init(default_params);
	}

	init(params={}) {
		super.init(params);

		// initialize it's sensors
		let brain = this.brain;
		this.actuators = new (class extends BrainIO{
			constructor()  { super(brain,0,5); }
			get done()     { return this.asBool(0); }
			get readPage() { return BigMath.abs(this.asFloat(1)); }			
			get predChg()  { return this.asInt(2); }
			get predStart(){ return this.asRange(3,30); }
			get predFin()  { return this.asRange(4,30); }
		})();
		let actuators = this.actuators;

		this.sensors = new (class extends BrainIO{
			constructor(){ super(brain, actuators.length, default_params.comp_size+2); }
			// the company we are analyszing and the date of analysis
			set comp(v) { this[0] = BigInt(v); }
			set date(v) { 
				if(v instanceof Date){
					v = utils.DateToDay(v);
				}
				v = BigInt(v);
				this[1] = v; 
			}
			get comp()  { return this.asInt(0); }
			get date()  { 
				let d = this.asRange(1, 100000000); 
				d = utils.DayToDate(d); 
				return d; 
			}
			set envdata(v) {
				// needs to be offset by 2 to leave room for comp and date
				let statesize = this.envdata.length;
				let insize = v.length;
				for(let i=0; i<insize; i++){
					let pos = i % statesize;
					this[pos+2] = v[pos];
				}
			}
			#envdata = null;
			get envdata(){
				if (this.#envdata) return this.#envdata;

				// calculate the size of the envdata subset
				this.#envdata = new BigUint64Array(this.buffer,2*BigUint64Array.BYTES_PER_ELEMENT);
				return this.#envdata;
			}
		})();

		this.age = this.experience.length;
		this.experience.negative = 0;
		this.experience.positive = 0;
		for(let e of this.experience){
			if(e < 0){
				this.experience.negative += e;
			}
			else{
				this.experience.positive += e;
			}
		}

		if(this.age === 0){
			this.actuators.reset();
		}
	}

	get MAX_AGE(){
		return super.MAX_AGE;
	}

	clone() {
		const Bug = this.constructor;
		let genome = this.genome.clone();
		let bug = new Bug(genome);
		return bug;
	}

	/**
	 * Converts the current life to a deserialziable object
	 *
	 * The resulting object is a serialized verions, and can be used to 
	 * reconstitute the object in its current state. The object will survive 
	 * `JSON.stringify`/`parse` to ensure it can be written to disk.
	 *
	 * @see fromObject
	 *
	 * @returns Object Serializable
	 */
	toObject(){
		let obj = super.toObject();
		obj = Object.assign(obj,this);

		obj.sensors = {
			comp: obj.sensors.comp,
			date: obj.sensors.date
		};
		
		obj.actuators = {
			done: obj.actuators.done,
			predChg: obj.actuators.predChg,
			predStart: obj.actuators.predStart,
			predFin: obj.actuators.predFin,
		};
		obj.brain = obj.brain.toObject();
		obj.gatetypes = Array.from(obj.gatetypes).map(f=>f.toString());
		obj.genome = obj.genome.toString();
		obj.experience = obj.experience.slice();

		return obj;
	}

	static fromString(json) {
		let obj = JSON.parse(json);
		let bug = SecAnimatOneComp.fromObject(obj);
		return bug;
	}

	get isDone(){
		let done = super.isDone;
		if(!done){
			//done = done || this.actuators.done;
			if(done){
				setTimeout(()=>{this.emit('complete');});
				return true;
			}
		}
		return done;
	}

	/**
	 * Reading the reality based on actuator outputs
	 * 
	 * The animat is able to set some desired parameters as its actuators, 
	 * namely which record of the universe it wants to read. THis is given 
	 * as a numeric record index. This index is looked up from the universe's 
	 * currently loaded company and copied into the animats brain.
	 * 
	 * It also re-affirms the company CIK and latest date.
	 * 
	 * @param {Company} perception 
	 */
	measure(perception){
		super.measure();

		// read the company and date off the book
		this.sensors.comp = perception.company;
		this.sensors.date = perception.latestDay;

		// find how many total record pages there are
		let pages = BigInt(perception.body.length);
		let page = [];
		if(pages <= 0){
			console.error('Read of company resulted in no records');
			debugger;
		}
		else{
			// ask the animat which page it wants to turn to
			page = this.actuators.readPage;
			// modulus for safety
			page = BigMath.floor(page * pages);
			page %= pages;
			page = Number(page);
			// get that page from the book
			page = perception.body[page];
		}
		// read it into sensor memory
		this.sensors.envdata.set(page,0);
		// fill the rest of the memory with nulls
		this.sensors.envdata.fill(0n,page.length);
	}
}

LifeForm.REGISTRY.set('SecAnimatOneComp',SecAnimatOneComp)
