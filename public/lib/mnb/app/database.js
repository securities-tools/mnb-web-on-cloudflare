import mnbdb from "../database.js";
import Genome from "../brain/genome.js";


import jsonBigint from 'https://cdn.jsdelivr.net/npm/json-bigint@1.0.0/+esm';
const JSONBig = jsonBigint({strict:true, useNativeBigInt:true, alwaysParseAsBig: true});

export default class CloudFlareDB extends mnbdb{

	constructor(db, opts={}){
		super(null, opts);
		this.db = '//'+location.host;
		if(db){
			db = new URL(db);
			this.db = db.host;
		}
	}

	destroy(){
		console.log('Destroy of shared space is disabled on Cloudflare');
	}

	readState(){
		return {};
	}

	async getBug(gufPos=null){
		await WaitPaused();
		let resp = await fetch({url:`${this.db}/database/breeder?pos=`,method:'GET'});
		let gen = resp.blob();
		get = new Genome(gen);
		return gen;
	}

	async saveBug(bug){
		let resp = {
			done: bug.actuators.done,
			predict: bug.actuators.predict,
			predChg: bug.actuators.predChg,
			predStart: bug.actuators.predStart,
			predFin: bug.actuators.predFin,
		};

		let blob = bug.genome.blob;
		let rec = {
			type: bug.type,
			genome: {
				parents: Array.from(bug.genome.parents.values()),
				karma: bug.genome.karma,
				generation: bug.genome.generation
			},
			bug: {
				id: null,
				req: bug.sensorConstants,
				resp: resp,
				age: bug.age,
			},
			asat: Date.now(),
		};
		rec.bug.id = bug.id;

		let form = new FormData();
		form.append('genome', blob);
		form.append('meta', JSONBig.stringify(rec));

		const response = await fetch(`/evolve/breeder`, {
			method: "PUT",
			body: form,
		});
		let text = await response.text();
		console.log(`${response.status}: ${text}`.substring(0,100));
		if(!([200,409].includes(response.status))){
			this.emit('error',{status:response.status,message:text.msg});
		}
	}

	emit(event,detail=this){
		if(!(event instanceof Event)){
			event = new CustomEvent(event,{detail:detail});
		}
		this.dispatchEvent(event);
	}

	/**************
	 * CLEAN THE GUFF
	 *
	 * At some point, we need to get rid of items
	 * that are not going to do anything. This is
	 * purely to conserve space.
	 */
	async cleanGuf(){
		try{
			isPausedFlag = true;
			const response = await fetch(url, {method: "DELETE"});
		}
		catch(e){
			debugger;
			console.error(e);
		}
		finally{
			isPausedFlag = false;
		}
	}


	async getDbSize(){
		if(this._dbsize !== null) return this._dbsize;

		this._dbsize = new Promise(async (resolve)=>{
			async function tryit(){
				try{
					let size = await db.db.facts.info();
					resolve(size.doc_count);
				}
				catch(e){
					if(e.code !== 'ECONNRESET'){
						let size = {doc_count:0};
						resolve(size.doc_count);
					}
					setTimeout(tryit,16);
				}
			}
			tryit();
		});

		return this._dbsize;
	}
}
