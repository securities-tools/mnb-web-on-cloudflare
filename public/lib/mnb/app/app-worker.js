import Boltzy from './app.js';

let multiverse = {universe:null,boundverse:0};


let bindEventsInterval = null;
function BindEvents(terranium){
	if(terranium === multiverse.boundverse) return;
	multiverse.boundverse = terranium;

	clearInterval(bindEventsInterval);

	terranium.addEventListener('progress',(e)=>{
		postMessage({
			type:'progress',
			data:e.detail
		});
	});
	terranium.addEventListener('bugdone',(e)=>{
		postMessage({type:'bugdone'});
	});
	terranium.addEventListener('UniverseTerminate',()=>{
		bindEventsInterval = setInterval(()=>{
			BindEvents(multiverse.universe);
		},135);
		postMessage({type:'UniverseTerminate'});
	});
}


async function main(){
	console.log('Starting Web Workers');
	multiverse = await Boltzy();
	BindEvents(multiverse.universe);
}

onmessage = (e) => {
	switch(e.data.type){
		case 'start':
			break;
		case 'stop':
			break;
		default:
			break;
	}
};

main();
