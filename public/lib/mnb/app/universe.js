import * as kv from 'https://cdn.jsdelivr.net/npm/idb-keyval@6/+esm';

import LifeForm from "./animatSecOneComp.js";
import Evolve from '../evolve.js';
import Company from '../../library/company.js';
import * as utils from '../utils.js';

export {
	Universe as default
};

class Universe extends EventTarget{
	#lifeForms = new Set();
	#bugFinish = null;
	#evolve = null;
	#Reality = null;
	#destruction = 100;
	#countDown = this.#destruction;
	#capacity = 4;
	
	// 5 minutes = 300000ms
	static SaveInterval = 300000;
	static ProgInterval = 1000;

	constructor(settings) {
		super();

		// https://github.com/josdejong/workerpool
		this.pool = [];
		this.library = settings.db.facts;
		this.#capacity = settings.capacity || 10;
		this.#evolve = 	new Evolve(settings.db);
		this.#destruction = settings.destruction || this.#destruction;
		this.#countDown = this.#destruction;

		this.#bugFinish = (l)=>{
			this.emit('bugdone',l.detail);
		};

		this.addEventListener('bugdone',async (l)=>{
			if(this.#countDown === 2){
				console.log('Two Minutes to Midight');
			}
			this.#countDown--;
			if(this.#countDown > 0){
				await this.remove(l.detail);
			}
			else{
				await this.destroy();
			}
		});

		console.log(`${this.constructor.name}: Big Bang. Universe created`);

		this.isInitialized = this.Restore();
	}

	async destroy(){
		console.log(`${this.constructor.name}.destroy`);
		this.#capacity = -1;
		this.#countDown = -1;
		await this.clear();
		await kv.clear();
		this.emit('UniverseTerminate',this);
	}

	async emit(event,details){
		details = await details;
		if(!(event instanceof Event)){
			event = new CustomEvent(event, {detail:details});
		}
		console.debug(`${this.constructor.name}: emit '${event.type}'`);
		this.dispatchEvent(event);
	}

	get extinction(){
		return this.#countDown;
	}

	get capacity(){
		let c = Math.min(this.#countDown, this.#capacity);
		return c;
	}
	get availableSpace(){
		return this.capacity - this.#lifeForms.size;
	}

	get lifeForms(){
		return Array.from(this.#lifeForms.values());
	}

	async #WorkLogger(life){
		if(life.isDone){
			clearInterval(life.logger);
			clearInterval(life.progressor);
		}
		//let id = bug.id;
		//console.log(`Work..: ${id} -> ${bug.age} of ${bug.MAX_AGE} (${Math.ceil(bug.age * 100 / bug.MAX_AGE)}%)`);
		let prog = {
			lengthComputable:false,
			remaining:this.extinction,
			total:this.#destruction,
			lifes:Array.from(this.#lifeForms).map(d=>d.toObject()),
		};
		this.emit('progress',prog);
	}


	async add(life){
		console.log(`${this.constructor.name}.add: adding a new soul (${life.id})`);
		if (this.availableSpace <= 0){
			console.log(`${this.constructor.name}.add: aborting ... over capacity (${this.#lifeForms.size} of ${this.capacity})`);
			return this;
		}

		if(Array.isArray(life)){
			console.log(`${this.constructor.name}.add: splitting array into separate entities`);
			life.forEach(l=>this.add(l));
			return this;
		}
		if(!(life instanceof LifeForm)) return this;

		try{
			this.#lifeForms.add(life);
			this.checkpoint(life);
		}
		catch(e){
			console.error('Failed to save during `add`');
		}
		this.emit("add", life);
		life.addEventListener('complete',this.#bugFinish);
		setTimeout(()=>{this.tick(life);},100);
		life.progressor = setInterval(()=>{this.#WorkLogger(life)},Universe.ProgInterval);
		life.logger = setInterval(()=>{this.checkpoint(life)},Universe.SaveInterval);
		this.#WorkLogger(life);

		return this;
	}

	async remove(life){
		if(!life){
			debugger;
			return;
		}

		if(Array.isArray(life)){
			let lives = [];
			for(let l of life){
				lives.push(this.remove(l));
			}
			lives = await Promise.all(lives);
			return lives;
		}

		console.log(`${this.constructor.name}.remove(${life.id})`);
		await this.#WorkLogger(life);
		life.removeEventListener('complete',this.#bugFinish);
		let success = this.#lifeForms.delete(life);
		clearInterval(life.logger);
		clearInterval(life.progressor);
		delete life.logger;
		delete life.progressor;
		try{
			await this.checkpoint(life);
		}
		catch(e){
			console.debug('Failed to delete object');
		}
		if(success){
			this.emit('remove', life);
		}
		return life;
	}

	async clear(){
		await this.remove(this.lifeForms);
	}

	async tick(bug){
		if(!this.#lifeForms.has(bug)){
			return;
		}

		bug.measure(this.#Reality);
		bug.think();
		bug.act();

		setTimeout(()=>{this.tick(bug);});
	}

	toObject(){
		let obj = {};

		obj.reality = this.#Reality.toObject();
		obj.age = this.extinction;
		obj.capacity = this.capacity;
		obj.lifeForms = Array.from(this.#lifeForms).map(l=>l.toObject());

		return obj;
	}

	async checkpoint(bug){
		await this.isInitialized;
		console.log(`Universe.checkpoint(${bug.id}): start `);
		let asat = (new Date()).toISOString();

		// save the local universe state
		console.log(`Universe.checkpoint(${bug.id}): Saving Universe state`);
		let newval = this.toObject();
		delete newval.lifeForms;
		if(newval.age > 0){
			newval.asat = asat;
			await kv.set('universe', newval);
		}
		else{
			await kv.del('universe');
		}

		// save the identified bug
		console.log(`Universe.checkpoint(${bug.id}): Saving Animat`);
		newval = bug.toObject();
		if(this.#lifeForms.has(bug)){
			newval.asat = asat;
			await kv.set(`bug-${bug.id}`, newval);
		}
		else{
			await kv.del(`bug-${bug.id}`);
		}

		return this;
	}

	#isRestoring = false;
	async Restore(){
		console.log(`${this.constructor.name}.Restore`);
		while(this.#isRestoring){
			console.log(`${this.constructor.name}.Restore: waiting for other instance`);
			await utils.wait(135);
		}
		this.#isRestoring = true;

		console.debug(`${this.constructor.name}.Restore: lookup existing universe`);
		let settings = await kv.get('universe');
		if(settings && settings.age > 0){
			console.debug(`${this.constructor.name}.Restore: restoring universe`);
			this.#Reality = await Company.fromObject(settings.reality);
			this.#countDown = settings.age;
			this.#capacity = settings.capacity;
		}
		else{
			console.debug(`${this.constructor.name}.Restore: no universe to restore`);
			this.#Reality = await this.#evolve.definePurpose();
		}

		// Get all the documents and assume they are bug checkpoints
		let keys = Array.from(await kv.keys());
		keys = keys.filter((d)=>d.startsWith('bug-'));
		console.debug(`${this.constructor.name}.Restore: found ${keys.length}`);
		keys = keys.slice(0,this.availableSpace);
		console.debug(`${this.constructor.name}.Restore: room for ${this.availableSpace}, loading ${keys.length}`);
		for(let k of keys){
			let bug = await kv.get(k);
			console.debug(`${this.constructor.name}.Restore: found ${bug.id}`);
			bug = LifeForm.fromObject(bug);
			let parents = Array.from(bug.genome.parents).map(d=>d.toString(32).padStart(8,'0'));
			console.log(`Restoring genome ${bug.id} with parents ${parents}`);
			await this.add(bug);
		}

		this.#isRestoring = false;
		console.debug(`${this.constructor.name}.Restore: done.`);
		return this;
	}

}
