import Universe from './universe.js';

import Evolve from '../evolve.js';
import * as gates from '../brain/gates.js';
import mnbDatabase from './database.js';

let state;
let evolve;
let fp;

function loadParams(params = {}){
	let rtn = {
		progress: [1,1000],
		iterations: 20000,
		capacity: 2,
	};
	rtn = Object.assign(rtn,params);
	params = location.search
		.split('&')
		.map(d=>{
			return d.split('=');
		})
		.filter(d=>{
			return d.length >= 2;
		})
		.reduce((a,d)=>{
			a[d[0]] = d[1];
			return a;
		},{})
		;
	rtn = Object.assign(rtn,params);
	return rtn;
}


async function restore(curState={}){
	// TODO: Need to pull teh data from disk
	/*
    let state = db
		.transaction(['boltzey'], 'read')
		.objectStore('boltzey')
		.get('boltzeystate')
		;
	state = Object.assign(curState,state);
	return state;
	*/
	return curState;
}

async function addBug(terranium){
	let bug = await evolve.pullFromGuf();
	await terranium.add(bug);
	let id = bug.id;
	console.log(`Born..: ${id} -> ${bug.genome.generation}`);
	return bug;
}



async function CreateUniverse(){

	state = await restore();
	state = loadParams(state);
	state.gatetypes = gates.GateTypes.bitGateTypes;
	state.db = new mnbDatabase();
	evolve = new Evolve(state.db);

	let terranium = new Universe(state);
	await terranium.isInitialized;

	terranium.addEventListener('bugdone',async (event)=>{
		let bug = event.detail;
		let id = bug.id;
		console.log(`Died..: ${id} -> ${bug.age}`);
		evolve.seedGuf(bug);
		setTimeout(FillTerranium,100+Math.random()*100);
	});

	terranium.addEventListener('UniverseTerminate',async (e)=>{
		terranium = null;
		setTimeout(main,1000);
	});

	async function FillTerranium(){
		// then create new bugs if necessary
		if(!terranium) return [];
		let newbugs = terranium.availableSpace;
		newbugs = Math.max(newbugs, 0);
		newbugs = Array(newbugs).fill(addBug).map(d=>d(terranium));
		newbugs = await Promise.all(newbugs);
		return newbugs;
	}

	await FillTerranium();
	return terranium;
}

let multiverse = {universe:null};
export default async function main(){
	multiverse.universe = await CreateUniverse();
	return multiverse;
}
