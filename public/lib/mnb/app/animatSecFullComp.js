import {LifeForm,BrainIO} from '../lifeform.js';
import * as utils from '../../utils.js';

// The original plan was to keep each brain at about 1MB with the company 
// data being kept separately in some shared space. This started to get 
// complicated, so instead I am having the company data in the brain itself. 
// In order to do this, we are going to set aside 24MB for the company 
// data + the 1MB for the thinking part itself.
const default_params = {comp_size:24*1024*1024};
default_params.min_brainSize = default_params.comp_size + 1024;
default_params.max_brainSize = default_params.min_brainSize + 1024**2;

export default class SecAnimatFullComp extends LifeForm{

	constructor(genome=null) {
		super(genome,default_params);

		if(!this.genome.parents.size){
			throw new Error('Faulty Genome. Parents not present, and are required for '+ this.constructor.name);
		}

		this.type = this.constructor.name;
		//this.init(default_params);
	}

	init(params={}) {
		super.init(params);

		// initialize it's sensors
		let brain = this.brain;
		this.actuators = new (class extends BrainIO{
			constructor()  { super(brain,0,20); }
			get done()     { return this.asBool(0); }
			get predChg()  { return this.asInt(1); }
			get predStart(){ return this.asRange(2,30); }
			get predFin()  { return this.asRange(3,30); }
		})();
		let actuators = this.actuators;

		this.sensors = new (class extends BrainIO{
			constructor(){ super(brain, actuators.length, default_params.comp_size); }
			// the company we are analyszing and the date of analysis
			set comp(v) { this[0] = v; }
			set date(v) { 
				if(v instanceof Date){
					v = utils.DateToDay(v);
				}
				this[1] = v; 
			}
			get comp()  { return this.asInt(0); }
			get date()  { return utils.DayToDate(this[1]); }
			set envdata(v) {
				// needs to be offset by 2 to leave room for comp and date
				let statesize = this.length - 3;
				let insize = v.length;
				for(let i=0; i<insize; i++){
					let pos = i % statesize;
					this[pos+2] = v[pos];
				}
			}
			get envdata(){
				return this.subarray(2,this.length+2-1);
			}
		})();

		this.age = this.experience.length;
		this.experience.negative = 0;
		this.experience.positive = 0;
		for(let e of this.experience){
			if(e < 0){
				this.experience.negative += e;
			}
			else{
				this.experience.positive += e;
			}
		}

		if(this.age === 0){
			this.actuators.reset();
		}
	}

	get MAX_AGE(){
		return super.MAX_AGE;
	}

	clone() {
		const Bug = this.constructor;
		let genome = this.genome.clone();
		let bug = new Bug(genome);
		return bug;
	}

	/**
	 * Converts the current life to a deserialziable object
	 *
	 * The resulting object is a serialized verions, and can be used to 
	 * reconstitute the object in its current state. The object will survive 
	 * `JSON.stringify`/`parse` to ensure it can be written to disk.
	 *
	 * @see fromObject
	 *
	 * @returns Object Serializable
	 */
	toObject(){
		let obj = super.toObject();
		obj = Object.assign(obj,this);

		obj.sensors = {
			comp: obj.sensors.comp,
			date: obj.sensors.date
		};
		
		obj.actuators = {
			done: obj.actuators.done,
			predChg: obj.actuators.predChg,
			predStart: obj.actuators.predStart,
			predFin: obj.actuators.predFin,
		};
		obj.brain = obj.brain.toObject();
		obj.gatetypes = Array.from(obj.gatetypes).map(f=>f.toString());
		obj.genome = obj.genome.toString();
		obj.experience = obj.experience.slice();

		return obj;
	}

	static fromString(json) {
		let obj = JSON.parse(json);
		let bug = SecAnimatOneComp.fromObject(obj);
		return bug;
	}

	#constant = null;
	measure(){
		if(!this.#constant){
			this.#constant = new BigUint64Array(this.sensors.length);
			this.#constant.set(this.sensors);
		}
		this.sensors.set(this.#constant);
		this.emit('measure');
	}
	think(){
		if(this.isDone){
			return;
		}
		this.brain.think(this.sensors);
		this.sensors.set(this.#constant);
		this.emit('think');
	}

	get isDone(){
		let done = super.isDone;
		if(!done){
			//done = done || this.actuators.done;
			if(done){
				setTimeout(()=>{this.emit('complete');});
				return true;
			}
		}
		return done;
	}

	act(){
		super.act();
	}

}

LifeForm.REGISTRY.set('SecAnimatFullComp',SecAnimatOneComp)
