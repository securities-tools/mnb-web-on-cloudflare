import {stats} from './gatesStats.js';
import BigMath from '../../BigMath.js';

export{
	bitGateTypes,
	stats as statsGateTypes,
	GateTypes as default,
	GateTypes
}


const bitGateTypes = [
	xor,
	or,
	and,
	nor,
	nand
];
//const GateTypes = new gateTypes();
const MaxMask = 2**32-1;
const GateTypes = new Set(bitGateTypes);
GateTypes.union(new Set(stats));


function xor(g1, state) {
	let sum = 0n;
	let maxsize = state.length;
	for (let i of g1.inputs) {
		i %= maxsize;
		let s = state[i];
		sum ^= s;
	}
	if(Number.isNaN(sum)){
		debugger;
	}
	return sum;
}


function or(g1, state) {
	let sum = 0n;
	let maxsize = state.length;
	for (let i of g1.inputs) {
		i %= maxsize;
		let s = state[i];
		sum |= s;
	}
	if(Number.isNaN(sum)){
		debugger;
	}
	return sum;
}


function and(g1, state) {
	//let sum = MaxMask;
	let sum = BigInt(2**32-1);
	let maxsize = state.length;
	for (let i of g1.inputs) {
		i %= maxsize;
		let s = state[i];
		sum &= s;
	}
	if(Number.isNaN(sum)){
		debugger;
	}
	return sum;
}


function nor(g1, state) {
	let sum = 0n;
	let statelen = state.length;
	for (let i of g1.inputs) {
		i %= statelen;
		let s = state[i];
		s = ~s;
		s = BigMath.abs(s);
		sum |= s;
	}
	if(Number.isNaN(sum)){
		debugger;
	}
	return sum;
}


function nand(g1, state) {
	//let sum = MaxMask;
	let sum = 2n**32n-1n;
	let statelen = state.length;
	for (let i of g1.inputs) {
		i %= statelen;
		let s = state[i];
		s = ~s;
		s = BigMath.abs(s);
		sum &= s;
	}
	if(Number.isNaN(sum)){
		debugger;
	}
	return sum;
}
