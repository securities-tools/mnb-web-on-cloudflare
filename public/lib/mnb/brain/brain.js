import { GateTypes } from './gates.js';
import * as utils from '../utils.js';
import BigMath from '../../BigMath.js';

export{
	Brain as default,
	Brain,
	BrainIO
};

const defaults = {};
defaults.min_brainSize = 1024 / BigUint64Array.BYTES_PER_ELEMENT;
defaults.max_brainSize = defaults.min_brainSize**3;
defaults.min_number_gates = defaults.min_brainSize;
defaults.max_number_gates = defaults.min_number_gates*2;
defaults.min_number_inputs = 1;
defaults.max_number_inputs = defaults.min_brainSize;
defaults.min_number_outputs = 1;
defaults.max_number_outputs = 4;

const ElementMax = 2n ** (BigInt(BigUint64Array.BYTES_PER_ELEMENT)*8n-1n);

class BrainIO extends BigUint64Array{
	constructor(brain, offset, elements){
		offset *= BrainIO.BYTES_PER_ELEMENT;
		super(brain.state.buffer,offset,elements);
	}

	reset(value=ElementMax){
		value = BigInt(value);
		this.fill(value);
	}

	static get ElementMax(){return ElementMax;}
	get ElementMax(){return ElementMax;}

	asUInt(pos)   { return BrainIO.toUInt(this[pos]); }
	asInt(pos)   { return BrainIO.toInt(this[pos]); }
	asFloat(pos) { return BrainIO.toFloat(this[pos]); }
	asBool(pos)  { return BrainIO.toBoolean(this[pos]); }
	asRange(pos,size) { return BrainIO.toRange(this[pos],size); }

	static toUInt(value){
		let v = value;
		return v;
	}

	static toInt(value){
		let v = value;
		v -= ElementMax;
		return v;
	}

	static toFloat(value){
		let v = value;
		v = BrainIO.toInt(v);
		v = v / ElementMax;
		return v;
	}

	static toRange(value,size=100){
		let v = BrainIO.toFloat(value);
		v = (v+1n)/2n;
		v *= BigInt(size);
		v = BigMath.round(v);
		return v;
	}

	static toBoolean(value){
		let v = value;
		v /= ElementMax;
		v *= 2n;
		v -= 1n;
		v = !!v;
		return v;
	}

}

class Brain{
	constructor(genome, gates=GateTypes.bitGateTypes, params={}){

		let p = Object.assign({}, defaults);
		p = Object.assign(p, params);

		this.state = this.#CreateMemory(genome, p);
		this.gates = this.#CreateGates(genome, this.state, gates);
	}


	toObject(){
		let state = this.state;
		state = new Uint8Array(state.buffer);
		state = utils.UInt8ToB64(state);
		let rtn =  {
			type: this.constructor.name,
			state: state,
			gates: this.gates.map(d=>d.toObject()),
		}
		return rtn;
	}


	static fromObject(obj, gatetypes=GateTypes.bitGateTypes){
		let state = utils.B64ToUInt8(obj.state);
		state = new BigUint64Array(state.buffer);

		let brain = new Brain([]);
		brain.state = state;

		let gates = obj.gates;
		brain.gates = gates.map(g=>Gate.fromObject(g,gatetypes));

		return brain;
	}


	think(sensorData=[]) {
		let mind = this.state;
		for (let k = 0; k < sensorData.length; k++) {
			mind[k] = sensorData[k];
		}

		for (let gate of this.gates) {
			let size = mind.length;
			let result = gate.handler(gate, mind);
			result = BigMath.floor(result);
			for (let o of gate.outputs) {
				o %= size;
				mind[o] = result;
			}
		}

		return mind;
	}

	get hash(){
		let rgbSize = Math.pow(2,3*8);
		let rgb = utils.HashArray(this.state, rgbSize);
		return rgb;
	}


	#CreateGates(genome,memory,gateTypes) {
		let qty = genome.nextRatio;
		qty *= defaults.max_number_gates - defaults.min_number_gates;
		qty += defaults.min_number_gates;
		qty = Math.floor(qty);

		let gates = [];
		for(let i = 0; i<qty; i++){
			let gate = new Gate(genome,memory,gateTypes);
			gates.push(gate);
		}
		return gates;
	}


	#CreateMemory(genome, params) {
		params.max_brainSize = Math.max(params.max_brainSize, params.min_brainSize);

		let qty = genome.nextRatio;
		qty *= params.max_brainSize - params.min_brainSize;
		qty += params.min_brainSize;
		qty = Math.floor(qty);

		let buf = 0;
		let states = new BigUint64Array(qty);
		for (let k = 0; k < qty; k++) {
			buf = genome.next;
			states[k] = BigInt(buf);
		}

		return states;
	}
}



/**
 * Represent a transform that reads from neurons and writes to other neurons
 *
 * A neuronal gate is a function that takes a list of neurons (input and
 * output), reads from the input neurons, and writes the result to the
 * output neurson. It is important to note that it does not take values
 * as input, but rather the memory registers (neurons) from which to read.
 *
 * In `C` parlance, it takes pointers.
 *
 * Creation of a Gate, is completely deterministic based on the Genome,
 * and does nto change over lifespan of the object. Once created, the
 * object will never change state. This becomes relevant during serialization:
 * there is no reason to serialize this object, it can be deterministically
 * recreated based on the genome.
 */
class Gate {

	constructor(genome,memory,gateTypes){
		gateTypes = Array.from(gateTypes);

		this.gatetype = genome.nextRatio;
		this.gatetype *= gateTypes.length;
		this.gatetype = Math.floor(this.gatetype);

		if(memory.length > 0){
			this.inputs = this.#wireNeurons(genome, memory.length, defaults.min_number_inputs, defaults.max_number_inputs);
			this.outputs = this.#wireNeurons(genome, memory.length, defaults.min_number_outputs, defaults.max_number_outputs);
		}

		this.handler = gateTypes[this.gatetype];
	}

	toObject(){
		let rtn = {
			type: this.constructor.name,
			handler: this.handler.toString(),
			handlerType: this.gatetype,
			inputs: null,
			outputs: null,
		};
		[rtn.inputs,rtn.outputs] = [this.inputs,this.outputs].map(d=>{
			let out = utils.UInt8ToB64(new Uint8Array(d.buffer));
			return out;
		});
		return rtn;
	}

	static fromObject(obj,gatetypes=GateTypes){

		let rtn = new Gate({nextRatio:0}, [], gatetypes);
		rtn.gatetype = obj.handlerType;
		rtn.handler = eval(`(${obj.handler})`);
		let allgates = Array.from(gatetypes).reduce((a,d)=>{a[d.name]=d;return a;},{})
		if(rtn.handler.name in allgates){
			rtn.handler = allgates[rtn.handler.name];
		}

		[rtn.inputs,rtn.outputs] = [obj.inputs,obj.outputs].map(d=>{
				let u8 = utils.B64ToUInt8(d);
				let len = u8.length;
				len = Math.floor(len/Uint32Array.BYTES_PER_ELEMENT)*Uint32Array.BYTES_PER_ELEMENT;
				u8 = u8.slice(0,len);
				let u32 = new Uint32Array(u8.buffer);
				return u32;
			});

		return rtn;
	}

	#wireNeurons(genome, mindSize, minWirings, maxWirings) {
		let qty = genome.nextRatio;
		qty *= maxWirings - minWirings;
		qty += minWirings;
		qty = Math.floor(qty);
		// it must always be at least one, or there is not pointers
		// so just add one to it.
		qty++;

		let wirings = [];
		for(let i = 0; i<qty; i++){
			let w = genome.nextRatio;
			w *= mindSize;
			w = Math.floor(w);
			wirings.push(w);
		}
		wirings = new Uint32Array(wirings);
		return wirings;
	}
}
