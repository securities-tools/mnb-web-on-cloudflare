import BigMath from "../../BigMath.js";

export {
	stats as default,
	stats,
}

const stats = [
	Count,
	Sum,
	Mean,
	Median,
	mode,
	stddev,
	absdev,
	min,
	max,
];


function Count(g) {
	return g.inputs.length;
}


function Sum(g, state) {
	let sum = 0n;
	for (let i of g.inputs) {
		i %= state.length;
		let s = state[i]
		sum += s;
	}
	if(Number.isNaN(sum)){
		debugger;
	}
	return sum;
}

function Mean(g,state){
	let count = BigInt(g.inputs.length);
	let avg = 0n;
	let statelen = state.length;
	for (let i of g.inputs) {
		i %= statelen;
		let s = state[i]
		s /= count;
		avg += s;
	}
	if(Number.isNaN(avg)){
		debugger;
	}
	return avg;
}

function Median(g,state){
	try{
		// copy all the values from memory into a new array
		let values = Array.from(g.inputs);
		for(let v in values){
			let index = values[v];
			index %= state.length;
			values[v] = state[index];
		}
		// sort it
		values = values.sort((a,b)=>{return Number(BigMath.sign(a-b));});
		// find the middle and grab the middle value from the top and the bottom.
		let mid = Math.ceil((values.length)/2)-1;
		let low = values[mid];
		mid = values.length-1-mid; 
		let high = values[mid];
		// find their average
		let median = (high+low)/2n;
		return median;
	}
	catch(e){
		debugger;
		return 1/0;
	}
}

function mode(g,state){
	let values = new Map();
	let maxsize = g.inputs.length;
	for(let pos of g.inputs){
		let value = state[pos%maxsize];
		if(!values.has(value)){
			values.set(value,0);
		}
		values.set(value,values.get(value)+1);
	}
	values = Array.from(values.entries());
	values.sort((a,b)=>{
		return a[1]-b[1];
	});
	let mode = [values.pop()];
	for(let val=values.pop(); val; val= values.pop()){
		if(mode[0][1] !== val[1]){
			break;
		}
		mode.push(val);
	}
	mode = mode.map(m=>{return m[0];});
	maxsize = BigInt(mode.length);
	mode = mode.reduce((a,d)=>{
		a += d/maxsize;
		return a;
	},0n);
	if(Number.isNaN(mode)){
		debugger;
	}
	return mode;
}

function stddev(g,state){
	try{
		let mean = Mean(g,state);
		let dev = 0n;
		let size = g.inputs.length;
		for (let s of g.inputs) {
			s %= size;
			let diff = state[s];
			diff -= mean;
			diff  = BigMath.pow(diff,2);
			diff /= BigInt(size);
			dev += diff;
		}
		dev = BigMath.pow(dev,0.5);
		return dev;
	}
	catch(e){
		debugger;
	}
}

function absdev(g,state){
	let mean = Mean(g,state);
	let statelen = state.length;
	let gatelen = BigInt(g.inputs.length);
	let dev = 0n;
	for (let s of g.inputs) {
		s %= statelen;
		let diff = state[s];
		diff -= mean;
		diff = BigMath.abs(diff);
		diff /= gatelen;
		dev += diff;
	}
	if(Number.isNaN(dev)){
		debugger;
	}
	return dev;
}

function min(g,state){
	try{
		let m = (2n ** BigInt(BigUint64Array.BYTES_PER_ELEMENT*8));
		let statelen = state.length;
		for(let s of g.inputs){
			s %= statelen;
			let v = state[s];
			m = BigMath.min(m,v);
		}
		if(Number.isNaN(m)){
			throw isNaN;
		}
		return m;
	}
	catch(e){
		debugger
	}
}

function max(g,state){
	let m = -1n * (2n ** BigInt(BigUint64Array.BYTES_PER_ELEMENT*8));
	let statelen = state.length;
	for(let s of g.inputs){
		s %= statelen;
		let v = state[s];
		m = BigMath.max(m,v);
	}
	if(Number.isNaN(m)){
		debugger;
	}
	return m;
}

