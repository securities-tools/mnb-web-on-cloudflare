import * as utils from '../utils.js';

const lenMeta = 10;
const mutation_rate = 0.003;
const mutation_scale = 0.001;
const lenGenome = (1024**2 - lenMeta) / Uint32Array.BYTES_PER_ELEMENT; // 1MB
//const lenGenome = 1024 / Uint32Array.BYTES_PER_ELEMENT;


/**
 * A genome
 *
 * The Genome is a sequence of instructions that are used to create a
 * lifeform.
 *
 * The first 10 elements are used for meta data
 *
 * - `0`: the generation
 * - `1`: karma
 * - `2`: id - a hashed value of the entire genome
 * - `3`-`9`: parents - the ids of parents that were mated to create this genome
 */
export default class Genome extends Uint32Array {
	constructor(genes=null){
		genes = genes || Math.floor(Math.random()*Genome.MAX_GENOMESIZE);

		if(genes instanceof ArrayBuffer){
			// this is perfect, this is what we want to get our datatype to
			// anything else we received should be converted to this datatype
		}
		else if(genes.buffer && genes.buffer instanceof ArrayBuffer){
			genes = genes.buffer;
		}
		else if(typeof genes === 'string'){
			let bytes = utils.B64ToUInt8(genes);
			genes = bytes.buffer;
		}
		else if(typeof genes === 'number'){
			genes = Math.min(genes,Genome.MAX_GENOMESIZE);
			genes = Math.max(genes,Genome.MIN_GENOMESIZE);
			genes = genes * Genome.BYTES_PER_ELEMENT;
			//console.log(`Genome Created with number ${genes} of ${Genome.MAX_GENOMESIZE} with ${Genome.BYTES_PER_ELEMENT} bytes per element`);
		}
		else{
			throw Error(`Genome creation from invalid data type: ${genes.constructor.name}`);
		}

		if(genes instanceof ArrayBuffer){
			genes = new Uint8Array(genes);
			while(genes.byteLength < Genome.MIN_BYTESIZE){
				let newgenes = new Uint8Array(genes.byteLength*2);
				newgenes.set(genes,0);
				newgenes.set(genes,genes.length);
				genes = newgenes;
			}
			if(genes.byteLength > Genome.MAX_BYTESIZE){
				genes = genes.slice(0,Genome.MAX_BYTESIZE);
			}
			let imbalance = genes.byteLength%4
			//console.log(`Genome received with arraybuffer ${genes.byteLength} with ${imbalance} extra`);
			if(imbalance){
				genes = genes.slice(0,genes.length-imbalance);
			}
			genes = genes.buffer;
		}

		super(genes);
		if(this.length < Genome.MIN_GENOMESIZE){
			throw Error('Invalid Genome size created: '+this.length+' ('+Genome.MIN_GENOMESIZE+','+Genome.MAX_GENOMESIZE+') using '+genes.constructor.name+'');
		}

		// If the type is number, that means we just generated this thing.
		// There are some metadata values that should be initialized.
		if(typeof genes === 'number'){
			// set the metadata to 0
			for (let i=9; i >=0; i--){
				this[i] = 0;
			}
			for (let i=this.length-1; i >= 10; i--) {
				let r = Math.floor(Math.random() * Genome.MAX_GENE);
				this[i] = r;
			}
			// call this for the side effect of actually calculating the ID
			this.id;
			this.karma = 0;
		}
		this.pos = lenMeta;
		this.#parents = null;
		if(this.generation === 0) this.karma = 0;
	}

	static MAX_GENE = 2**(8*Genome.BYTES_PER_ELEMENT);
	static MAX_GENOMESIZE = Math.floor(lenGenome);
	static MAX_BYTESIZE = Math.floor(Genome.MAX_GENOMESIZE * Genome.BYTES_PER_ELEMENT);
	//static MIN_GENOMESIZE = 1024;
	static MIN_GENOMESIZE = Math.floor(lenGenome);
	static MIN_BYTESIZE = Genome.MIN_GENOMESIZE * Genome.BYTES_PER_ELEMENT;
	static META_SIZE = lenMeta;
	static PARENT_POS_MAX = 9;
	static PARENT_POS_MIN = 3;
	static PARENTS_MAX = Genome.PARENT_POS_MAX-Genome.PARENT_POS_MIN+1;

	get type(){
		return 'application/vius.genome';
	}

	#parents = null;
	get parents(){
		if(this.#parents) return this.#parents;

		this.#parents = new Set();
		for(let i=Genome.PARENT_POS_MIN; i<=Genome.PARENT_POS_MAX; i++){
			if(this[i]){
				this.#parents.add(this[i]);
			}
		}
		return this.#parents
	}
	set parents(v){
		let parents = Array.from(this.parents).sort((a,b)=>{return a-b;});
		let i = Genome.PARENT_POS_MIN;
		for(let parent of parents){
			this[i++] = parent;
		}
		for(; i<=Genome.PARENT_POS_MAX; i++){
			this[i] = 0;
		}
	}

	get karma(){
		/**
		 * Returns the Karma associated with this Genome as an signed integer
		 */
		let k = this[1];
		k -= this.GeneScale/2;
		return k;
	}

	set karma(k){
		/**
		 * Stores the karma (a signed integer) as an
		 */
		k = Math.ceil(k);
		if(k <= 0) k--;
		k += this.GeneScale/2;

		k = Math.max(k, 0);
		k = Math.min(k, this.GeneScale);

		this[1] = k;
	}

	get generation(){
		let k = this[0];
		return k;
	}

	set generation(g){
		this[0] = g;
	}

	get genome(){
		return new Uint32Array(this.buffer,40);
	}

	get id(){
		if (this[2] > 0) return this[2];
		// forces a recalculate
		this.id = null;
		// send the value back
		return this[2];
	}

	set id(v){
		this[2] = utils.HashArray(this.genome);
	}

	get meta(){
		return {
			parents: Array.from(this.parents),
			karma: this.karma,
			generation: this.generation,
			length: this.length,
			id: this.id,
		};
	}

	get META_SIZE(){
		return lenMeta;
	}

	get GeneScale(){
		return Genome.MAX_GENE;
	}
	get GeneScaleBits(){
		return 8*Genome.BYTES_PER_ELEMENT;
	}

	get peek(){
		let val = this.pos;
		val = this[val];
		val = Math.floor(val);
		return val;
	}
	get peekRatio(){
		let val = this.peek;
		val = val / this.GeneScale;
		return val;
	}

	get next(){
		let val = this.peek;
		this.pos++;
		return val;
	}
	get nextRatio(){
		let val = this.next;
		val /= this.GeneScale;
		return val;
	}

	/**
	 * When we set the position of the reader, we need to make sure it 
	 * never goes out of bounds. Whatever our advancing increment is, 
	 * it shoudl wrap around the genome and start reading fromthe beginning 
	 * again if necessary.
	 */
	#pos;
	get pos(){
		return this.#pos;
	}
	set pos(val){
		this.#pos = (Math.floor(val) + this.length) % this.length;
	}

	shift(){
		return this.next;
	}

	pop(){
		return this.next;
	}

	push(){
		console.warn("Illegal Operation: PUSH to GENOME");
	}

	unshift(){
		console.warn("Illegal Operation: UNSHIFT to GENOME");
	}

	slice(){
		let segment = new Uint32Array(this.buffer);
		segment = segment.slice(...arguments);
		return segment;
	}

	clone(){
		let gen = this.slice();
		gen = new Genome(gen);
		return gen;
	}

	mutate(rate=mutation_rate) {
		let genome = this;
		for (let m = Math.floor(this.length*rate); m>=0; m--) {

			let loc = Math.floor(Math.random() * this.length);
			// It is important that it not be 50%. Over time, we want
			// change to occur however, if the amount of change each time
			// is 50/50, the net change over time will be zero. Therefore
			// we need to favour one direction or the other.
			//  - It doesn't really matter which.
			//  - This is not an optimal solution.
			let val = (Math.random()-0.55)*2;
			val	*= this.MAX_GENE;
			val *= mutation_scale;
			val  = Math.floor(val);
			val += genome[loc];
			val += this.MAX_GENE;
			val %= this.MAX_GENE;
			genome[loc] = val;
		}
		genome.id = null;
		return genome;
	}

	mate(){
		let parents = Array.from(arguments);
		parents.push(this);
		let genome = Genome.mate(...parents);
		return genome;
	}

	static mate() {
		// get the arguments
		let parents = Array.from(arguments)
			// but make sure they are unique (as verified by calculated ID)
			.reduce((a,d)=>{
				a[d.id] = d;
				return a;
			},{})
			;
		let parentList = Object.keys(parents).map(d=>+d);
		parents = Object.values(parents);

		let parentsLength = parents.length;
		if(parentsLength <= 0){
			throw new Error('Cannot `mate` less than one genome');
		}
		//console.log(`MATE: ${parentsLength}`);
		for(let parent of parents){
			if(!(parent instanceof Genome)){
				throw new Error('Parameter is not a `Genome`: '+parent.constructor.name);
			}
		}

		//console.log(`MATE: ${JSON.stringify(parentList)}`);
		let rtn = parents.pop()
		rtn = rtn.clone();

		let generation = rtn.generation/parentsLength;
		let karma = rtn.karma/parentsLength;

		for(let parent of parents){

			parent = parent.clone();

			generation += parent.generation / parentsLength;
			karma += parent.karma / parentsLength;

			for (let pos=this.length-1; pos >= 0;) {
				let section_length = Math.randomIntFromInterval( lenGenome**0.5, lenGenome/2 );
				let copyside = Math.floor(Math.random() * 2);
				if(copyside % 2 === 0){
					pos -= section_length;
				}
				else for(let i=section_length; i>=0; i--,pos--){
					rtn[pos] = parent[pos];
				}
			}
		}

		// every new generation has some amount of mutation
		rtn.mutate();

		// Now ... who knows what got changed during all those numbers
		// make sure all the meta data is explicitely set to sensible values

		// set the generation and karma (both averages of their parents)
		rtn.karma = Math.ceil(karma);
		 // and advance the generation by one
		rtn.generation = Math.ceil(generation)+1;

		// reset the parent list of IDs
		//console.log(`MATE: Parents 1 ${JSON.stringify(Array.from(rtn.parents))}`);
		rtn.parents.clear();
		//console.log(`MATE: Parents 2 ${JSON.stringify(Array.from(rtn.parents))}`);
		for(let parent of parentList){
			rtn.parents.add(parent);
		}
		//console.log(`MATE: Parents 3 ${JSON.stringify(Array.from(rtn.parents))}`);
		rtn.parents = null;
		//console.log(`MATE: Parents 4 ${JSON.stringify(Array.from(rtn.parents))}`);
		// recalculate the ID number
		rtn.id = null;
		//console.log(`MATE: ID ${rtn.id}`);

		return rtn;
	}

	get buffer(){
		this.parents = null;
		return super.buffer;
	}

	toString(){
		let bytes = new Uint8Array( this.buffer );
		bytes = utils.UInt8ToB64(bytes);
		return bytes;
	}

	get blob(){
		return new Blob([this.buffer],{type:'application/vius.genome'});
	}
}



