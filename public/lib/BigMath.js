
function CastBigInt(val){
	if(typeof val === 'number'){
		val = Math.round(val);
	}
	val = BigInt(val);
	return val;
}

function stats(){
	let args = Array.from(arguments);
	let rtn = BigMath.round(args.pop());
	rtn = {min: rtn,max: rtn};

	while(args.length > 0){
		let candidate = args.pop();
		candidate = BigMath.round(candidate);

		if(rtn.min > candidate){
			rtn.min = candidate;
		}
		if(rtn.max < candidate){
			rtn.max = candidate;
		}
	}
	return rtn;
}

export default class BigMath{

	static abs(val){
		val = CastBigInt(val);
		if(val < 0n){
			val *= -1n;
		}
		return val;
	}

    static min(){
		return stats(...arguments).min;
	}

	static max(){
		return stats(...arguments).max;
	}

	static pow(base, exp){
		if(Number(exp) < 1 && Number(exp) > 0){
			exp = 1/exp;
			let p = BigMath.root(base, exp);
			return p;
		}

		base = BigInt(base);
		exp = BigInt(exp);

		let p = base ** exp;
		return p
	}

	static random(){
		return Math.random();
	}

	static root(base, exp) {
		base = BigInt(base); // Ensure input is BigInt
		exp = BigInt(exp); // Ensure exponent is BigInt
	
		if (base === 0n || base === 1n) return base; // Handle edge cases
		if (exp <= 0n) throw new Error("Exponent must be greater than 0.");
		if (base < 0n && exp % 2n === 0n) {
			throw new Error("Even roots of negative numbers are not supported.");
		}
	
		let x = 2n ** BigInt(Math.floor(base.toString(2).length / Number(exp))); // Initial guess
		while (true) {
			let xToExpMinus1 = x ** (exp - 1n); // x^(n-1)
			let y = ((exp - 1n) * x + base / xToExpMinus1) / exp; // Generalized Babylonian formula
			if (x - y <= 1n && y - x <= 1n) { // Convergence condition
				return y; // Return the result
			}
			x = y; // Update x for the next iteration
		}
	}

	static round(val){
		val = CastBigInt(val);
		return val;
	}

    static sign(val){
        val = BigInt(val);
        if(val < 0n){
            return -1n;
        }
        else if(val > 0n){
            return  1n;
        }
        return 0;
    }

	static sqrt(base){
		return BigMath.root(base,2);
	}

	static floor(val){
		if(typeof val === 'bigint') return val;
		if(Number.isNaN(val)) debugger;
		val = Math.floor(val);
		val = BigInt(val);
		return val;
	}

	static ceil(val){
		if(typeof val === 'bigint') return val;
		val = Math.ceil(val);
		val = BigInt(val);
		return val;
	}

}
