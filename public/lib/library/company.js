import * as utils from '../../lib/utils.js';
import BigMath from '../../lib/BigMath.js';


export default class Company extends EventTarget{

    constructor(cik=null, date = new Date(), days=Math.ceil(365.25*5)){
        super();

		console.debug(`${this.constructor.name}: created`);
		this.#cik = +cik;
		this.#dateRange[0] = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()));
		this.#dateRange[1] = new Date(this.#dateRange);
		this.#dateRange[1].setDate(this.#dateRange[1].getDate()-days);

		this.#bookmark = [+cik, date, days];
		this.#waitLoad = this.#Load(...this.#bookmark);
    }

	#bookmark;
	get bookmark(){
		return this.#bookmark;
	}

	#waitLoad;
	get waitLoad(){
		return this.#waitLoad;
	}

	#cik = 0;
	get company(){
		return this.#cik;
	}

	#dateRange = [null,null];
	get requestrange(){
		return this.#dateRange.map(d=>new Date(d));
	}

	#header = [];
	get header(){
		return this.#header;
	}

	#body = [];
	get body(){
		return this.#body;
	}

	#status = 0;
	get status(){
		return this.#status;
	}

	#message = null;
	get message(){
		return this.#message;
	}

	#totalFields = 0;
	get totalFields(){
		return this.#totalFields;
	}

	#totalElements = 0;
	#totalBytes =  0;
	get totalBytes(){
		return this.#totalBytes;
	}

	#filteredBytes = 0;
	get filteredBytes(){
		return this.#filteredBytes;
	}

	#latestDay;
	get latestDay(){
		return this.#latestDay;
	}

	#earliestDay;
	get earliestDay(){
		return this.#earliestDay;
	}

	#latestDate;
	get latestDate(){
		return this.#latestDate;
	}

	#earliestDate;
	get earliestDate(){
		return this.#earliestDate;
	}

	toObject(){
		let obj = {
			cik: this.company,
			date: this.latestDate.toISOString().split('T').shift(),
			range: this.latestDay - this.earliestDay,
		};
		return obj;
	}

	static async fromObject(obj){
		if(!obj) return null;
		let date = obj.date.split('-').map(d=>+d);
		date[1]--;
		date = new Date(Date.UTC(...date));
		let resp = new Company(obj.cik, date, obj.range);
		await resp.waitLoad;
		return resp;
	}


	async #Load(){

		console.debug(`${this.constructor.name}.#Load`);
		let company = this.#cik;

		let url = `/facts/company?cik=${company}`;
		console.debug(`${this.constructor.name}.#Load: ${url} `);
		let resp = await fetch(url);
		//let resp = await fetch(`https://pub-1ba7d5a7259c4e76865bd1bede90754a.r2.dev/company/1805385`);
		this.#status = resp.status;
		console.debug(`${this.constructor.name}.#Load: Received ${this.#status} `);
		if(resp.status !== 200){
			let msg = `Could not find ${company}`;
			console.error(msg);
			this.#message = msg;
			return rtn;
		}

		console.debug(`${this.constructor.name}.#Load: Set up reader `);
		let blob = await resp.arrayBuffer();
		blob = new BigUint64Array(blob);
		this.#totalBytes = blob.byteLength;

		let bookmark = 0;
		let ReadRec = (size)=>{
			let rec = blob.slice(bookmark, bookmark+size);
			bookmark += size;
			return rec;
		}

		console.debug(`${this.constructor.name}.#Load: Read the metadata`);
		let rec = ReadRec(5);
		rec = Array.from(rec).map(d=>Number(d));

		this.#totalFields = rec[1];
		this.#latestDay = utils.DateToDay(new Date(Math.max(...this.#dateRange)));
		this.#latestDay = Math.min(this.#latestDay, rec[3]);
		this.#earliestDay = utils.DateToDay(new Date(Math.min(...this.#dateRange)));
		this.#earliestDay = Math.max(this.#earliestDay, rec[2]);
		this.#latestDate = utils.DayToDate(this.#latestDay);
		this.#earliestDate = utils.DayToDate(this.#earliestDay);
		console.debug(`${this.constructor.name}.#Load: Metadata - TotalFields..: ${this.#totalFields}`);
		console.debug(`${this.constructor.name}.#Load: Metadata - Latest Day...: ${this.#latestDay} (${this.#latestDate.toISOString()})`);
		console.debug(`${this.constructor.name}.#Load: Metadata - Earliest Day.: ${this.#earliestDay} (${this.#earliestDate.toISOString()})`);

		console.debug(`${this.constructor.name}.#Load: Read the field headers`);
		this.#header = ReadRec(this.#totalFields);

		console.debug(`${this.constructor.name}.#Load: Build the body`);
		let first = BigInt(this.#earliestDay);
		let last = BigInt(this.#latestDay);
		this.#totalElements = (this.#totalBytes/blob.BYTES_PER_ELEMENT);
		for(let i=(last-first); i>=0; i--){
			rec = ReadRec(this.#totalFields)
			let recday = rec[0];
			if(recday <= last && recday >= first){
				this.#body.push(rec);
			}
			else if(recday > last){
				break;
			}
		}
		console.log("Company Loaded: "+ this.company);

		return this;
	}

}
