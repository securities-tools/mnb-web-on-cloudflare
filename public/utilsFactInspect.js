import Company from './lib/library/company.js';

const state = {
    data: null,
};


function AttachParts(){
	document.querySelector('input[name="company"]').addEventListener('change',LoadCompany);
	document.querySelector('#LoadComp').addEventListener('click',LoadCompany);
}


async function LoadCompany(){
	let company = document.querySelector('input[name="company"]');
	let rtn = new Company(company.value);
	rtn = await rtn.waitLoad;
	console.log("Company Loaded");

    state.data = rtn;
	RenderCompany(rtn);
	return rtn;
}


function RenderCompany(data=state.data, page=1, pagesize=12){
	if(!data || data.status !== 200){
		let msg = `Could not find ${data.company}`;
		console.error(msg);
		document
			.querySelector('thead')
			.innerHTML = `<tr><td>${msg}</td></tr>`
			;
		return;
	}

	document.querySelector('output[name="totalbytes"]').value = data.totalBytes;
	document.querySelector('input[name="company"]').value = data.company;
	document.querySelector('output[name="width"]').value = data.totalFields;
	document.querySelector('output[name="firstdate"]').value = data.earliestDate.toISOString().split('T').shift() + ' ('+data.earliestDay.toString(16).padStart(8,'0')+')';
	document.querySelector('output[name="lastdate"]').value  = data.latestDate.toISOString().split('T').shift() + ' ('+data.latestDay.toString(16).padStart(8,'0')+')';

	let tbody = document.querySelector('thead');
	tbody.innerHTML = '';
	let index = document.createElement('tr');
	let tr = document.createElement('tr');
	let pos = 0;
	for(let r of data.header){
		let th = document.createElement('th');
		th.textContent = r.toString(16).padStart(8,'0');
		tr.appendChild(th);
		th = document.createElement('th');
		th.textContent = pos++;
		index.appendChild(th);
	}
	tbody.appendChild(index);
	tbody.appendChild(tr);

	tbody = document.querySelector('tbody');
	tbody.innerHTML = '';
    let skip = (page-1)*pagesize;
    let cellsafety = 150000;
	for(let rec of data.body){
        if(skip-- > 0) continue;
        if(pagesize-- <= 0) break;
        if(cellsafety <= 0) break;

        let width = 15000;
		let tr = document.createElement('tr');
		tbody.appendChild(tr);
		for(let r of rec){
            if(width-- < 0) break;
			let td = document.createElement('td');
			td.textContent = r.toString(16).padStart(8,'0');
			tr.appendChild(td);
			cellsafety--;
		}
	}
	console.log("Company Rendered");
}


async function main(){
	AttachParts();
}


document.addEventListener('DOMContentLoaded',main);
