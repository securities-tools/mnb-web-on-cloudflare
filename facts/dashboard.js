

let someHost = "https://3a6f8a86-396c-43b0-a010-a2d951cbc30c-bluemix.cloudantnosqldb.appdomain.cloud/mnb";
let url = someHost + "/_design/score/_view/score?reduce=true&group=true";
let user = 'apikey-v2-15i49yhsabtanithar4ub5lxg2m5m1l78ehqypo037nw';
let pass = '4b06a411de94f9187e343285a9619717';
let token = btoa(`${user}:${pass}`);

let init = {
	headers: {
		"content-type": "application/json;charset=UTF-8",
		"Authorization": `Basic ${token}`
	}
};


export async function onRequest(context) {
	try {
		let data = await mnbCache.get("dashboard",{type:'json'});

		if(data === null){
			data = {
				asat: Date.now()
			};

			let results = await fetch(url,init);
			results = await results.json();
			data.scores = results.rows;

			mnbCache.put('dashboard', data, {
				// 15 min
				expirationTtl: 900+Math.random()*60
			});
		}


		let str = JSON.stringify(data);
		return new Response(str, {
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				'Access-Control-Allow-Headers': '*',
				'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
				'Access-Control-Allow-Origin': '*',
			},
			status: 200
		});
	}
	catch (err) {
		return new Response('Error parsing JSON content', { status: 400 });
	}
}

