import * as evolveBreeder from './evolve/breeder.js';
import * as factsCompany from './facts/company.js';
import * as factsGoals from './facts/goals.js';
import * as scores from './scores.js';

export {
	evolveBreeder,
	factsCompany,
	factsGoals,
	scores,
}
