let headers =  {
	'Access-Control-Allow-Headers': '*',
	'Access-Control-Allow-Methods': 'POST, OPTIONS',
	'Access-Control-Allow-Origin': '*',
};



const Scorer = {
	NullCik: 0
};
Scorer.MAX = 0xfffffffe/2;
Scorer.MIN = -Scorer.MAX;
Scorer.criteria = {};

Scorer.criteria.accuracy = {
	scale: 60000,
	calculation : (criteria)=>{
		return 0;
	}
};


/**
 *
 * @param animat
 */
export async function calcScore(animat){
	//console.debug('Evaluating performance');
	//console.debug(animat);
	let value = 0;
	let resp = animat.bug.resp;

	// reward ending the simulation prior to old age
	if(resp.done){
		//value += 10 * Math.max(0, Bug.MAX_AGE - (animat.bug.age||0) );
		value += Math.floor(0.0005 * (animat.bug.age-1));
	}

	// predicted a company? (or null, \uffff)
	if (resp.predict > Scorer.NullCik){
		value += 100;
	}
	else{
		return value;
	}
	/*
	let cik = resp.predict.toString(16).padStart(8,'0') + '-';
	let query = await this.db.facts.allDocs({
		startkey: cik,
		endkey: 'g',
		limit:1,
		reduce:false
	});
	query = query.rows.pop();
	if(query && query.id.startsWith(cik)){
		value += 100;
	}
	else{
		return value;
	}
	*/

	// not a particularly wide spread
	if(resp.predStart > resp.predFin){
		// doc points for having the range reversed
		let s = resp.predStart;
		resp.predStart = resp.predFin;
		resp.predFin = s;
		value -= 10;
	}
	// non-sense start date
	if(resp.predStart < 1){
		return value;
	}

	// grant points for the prediction being within a 60 day spread
	let spreadwidth = 15;
	let spread = resp.predFin - resp.predStart;
	spread = Math.max(spread, 0);
	spread = Math.min(spread, spreadwidth);
	spread = (spreadwidth - spread) / spreadwidth;
	value += 1000 * spread;

	// not too far in the future
	let startdist = 10;
	let startwait = resp.predStart;
	startwait = Math.max(startwait,0);
	startwait = Math.min(startwait,startdist);
	startwait = (startdist-startwait) / startdist;
	value += 1000 * startwait;

	// acurate prediction?
	/*
	query = {
		startkey: [resp.predict, resp.predStart],
		endkey: [resp.predict, resp.predFin],
		reduce:true
	};
	query = await this.db.facts.query('prices/pricerange',query);
	query = query.rows.pop();
	let accuracy = 0;
	if(resp.predChg < query.min){
		accuracy = resp.predChg - query.min;
		accuracy /= query.min;
	}
	else if(resp.predChg > query.max){
		accuracy = resp.predChg - query.max;
		accuracy /= query.max;
	}
	else{
		accuracy = 1;
	}
	value += this.MAX * accuracy;
	*/
	return value;
}


export async function onRequestPost(request, contextEnv, context) {
	let formData = await request.formData();

	let rec = JSON.parse(formData.get('meta'));
	rec.asat = Date.now();

	rec.bug.score = await calcScore(rec);

	headers['Content-Type'] = 'text/json; charset=utf-8';
	return new Response(JSON.stringify(rec), {
		status: 200,
		headers: headers,
	});
}


