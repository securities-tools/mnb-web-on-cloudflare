import Genome from '../../public/lib/mnb/brain/genome.js';
import { calcScore } from './judge.js';


let headers =  {
	'Content-Type': 'text/json; charset=utf-8',
	'Access-Control-Allow-Headers': '*',
	'Access-Control-Allow-Methods': 'GET, PUT, OPTIONS',
	'Access-Control-Allow-Origin': '*',
};

let env = null;
let context = null;


/**
 * Generates a random number distributed on a SIN wave
 *
 * THe shape of sin(0)->sin(0.5pi) is one of rising quickly and tapering off.
 */
function RandTopWieghted(){
	let x = Math.random();
	x *= 0.5;
	x *= Math.PI;
	let y = Math.sin(x);
	if(y < 0 || 1 < y){
		throw new Error(`Random range is outside range 0-1 (${y})`);
	}
	return y;
}


async function getBug(pos){

	pos = Math.floor(pos);
	pos = Math.min(pos,10000);
	let genomes = await env.EdgarBrains.list({prefix:'guf/'});

	if(genomes.objects.length === 0){
		// there is nothing in the Guf?
		// create the primordial genome.
		let genome = new Genome();
		await saveBug(genome, null, true);
		return genome;
	}

	let totalSize = genomes.objects.length;
	let discardSize = 0;
	while(genomes.truncated && totalSize < pos){
		discardSize+= genomes.objects.length;
		genomes = await env.EdgarBrains.list({cursor:genomes.cursor});
		totalSize+= genomes.objects.length;
	}

	let bug = null;
	if(totalSize<=pos){
		// its possible our request was larger than the dataset available
		// however, at this point we know how big the full dataset is
		// (i), therefore we can now adjust the requested position to
		// ensure it falls inside the available size. Sooo...
		pos = Math.abs(pos) % totalSize;
		bug = getBug(pos);
	}
	else{
		pos -= discardSize;
		bug = genomes.objects[pos];
		let key = bug.key;
		bug = await env.EdgarBrains.get(bug.key);
		bug = await bug.arrayBuffer();
		bug = new Genome(bug);
	}
	return bug;
}


async function pickRandomGenome(range=1000){
	let i = RandTopWieghted();
	i  = 1 - i;
	i *= range;
	i  = Math.floor(i);

	let gen = await getBug(i);
	return gen;
}


function makekey(id){
	let type = typeof id;
	if(type !== 'number'){
		throw new Error('Invalid data type in makekey ('+type+':'+id+')');
	}
	id = ('00000000'+id.toString(32))
		.split('')
		.reverse()
		.slice(0,7)
		.reverse()
		.join('')
		;
	return ['guf',id].join('/');
}


async function saveBug(genome, meta, force = false){
	genome = new Genome(genome);
	if(meta){
		meta.bug.score = await calcScore(meta);
		genome.karma = (genome.karma + meta.bug.score) / 2;
	}
	else{
		force = true;
		meta = meta ?? {
			genome: genome.meta,
			bug:{
				score: 0,
				id: '0000000.0000000.'+ ('0000000'+genome.id.toString(32)).split('').reverse().slice(0,7).reverse().join(''),
			}
		};
	}
	let key = makekey(genome.id);
	let parentKeys = JSON.stringify(Array.from(genome.parents).map(d=>d.toString(32)));
	console.log(`Saving new genome: ${key} (${meta.bug.score}/${genome.karma}) parents ${parentKeys}`);

	let parents = Array.from(genome.parents.values())
		.map(async (d)=>{
			d = makekey(d);
			let parent = env.EdgarBrains.get(d);
			return parent;
		});


	let exists = await env.EdgarBrains.head(key);
	exists = await exists;

	if(exists){
		return new Response('{"msg":"Genome already exists"}', {headers:headers, status:409} );
	}

	if(!meta.bug.score && meta.bug.score !== 0){
		return new Response('{"msg":"System not ready to handle predictions"}', {headers:headers, status:422} );
	}

	parents = await Promise.all(parents);
	for(let parent of parents){
		if(force) continue;
		if(!parent){
			return new Response(`{"msg":"Parent not found ${parentKeys}"}`, {headers:headers, status:412} );
		}
		parent.meta = JSON.parse(parent.customMetadata.meta);
		if(parent.meta.bug.ova <= 0){
			return new Response('{"msg":"Parent is sterile"}', {headers:headers, status:412} );
		}
	}
	// everything checks out. send it into the breeding pool and reduce
	for(let parent of parents){
		parent.meta.bug.ova--;
		let save = null;
		if(parent.meta.bug.ova <= 0){
			save = env.EdgarBrains.delete(parent.key);
		}
		else{
			save = env.EdgarBrains.put(parent.key, parent.body, {
				httpMetadata: parent.httpMetadata,
				customMetadata: {
					meta: JSON.stringify(parent.meta),
				}
			});
		}
		context.waitUntil(save);
	}

	// make sure it at least replaces its parents, and a little more
	meta.bug.ova = Math.floor(meta.bug.score/1000)+meta.genome.parents.length+1;
	let rec = await env.EdgarBrains.put(key, genome.buffer, {
		customMetadata: {
			meta: JSON.stringify(meta),
			httpMetadata: {
				contentType: 'application/vius-brain',
			},
			}
	});
	let msg = `Saved new Genome. ${key}x${meta.bug.ova} (${meta.bug.score}/${genome.karma})`;
	console.log(msg);
	return new Response(`{"msg":"${msg}"}`, {headers:headers} );
}


export async function onRequestGet(request, contextEnv, contxt) {
	env = contextEnv;
	context = contxt;
	// pick the number of parents to use
	let parents = RandTopWieghted();
	parents = Math.ceil(2 * parents);
	if(parents > 2){
		throw new Error('Attempt to create too many parents: '+parents);
	}
	parents = new Array(parents).fill(0).map(d=>pickRandomGenome());
	if(parents.length > 2){
		throw new Error('Attempt to use too many parents: '+parents.length);
	}
	// for each parent, breed them together
	parents = await Promise.all(parents);
	let genome = parents.pop();
	genome = genome.mate(...parents);
	// double check something stupid didn't happen
	if(genome.parents.size > 2) throw new Error('Created genome with too many parents: '+genome.parents.size);
	if(genome.parents.size < 1) throw new Error('Created genome with too few  parents: '+genome.parents.size);

	let parentKeys = JSON.stringify(Array.from(genome.parents).map(d=>d.toString(32)));
	console.log(`Sending genome ${genome.id} with parents ${parentKeys}\n`+
			Array.from(genome.slice(0,12)).map(d=>{return d.toString(16).padStart(8,'0')}).join('-')
		);
	headers['Content-Type'] = `${genome.type}; charset=utf-8`;
	headers['Content-Disposition'] = `attachment; filename="G-${genome.id}.genome"`;
	return new Response(genome.buffer, {
		headers:headers,
		status: 200
	});
}


export async function onRequestPut(request, contextEnv, contxt) {
	env = contextEnv;
	context = contxt;
	let req = request;
	let formData = await req.formData();

	let genome = formData.get('genome');
	genome = await genome.arrayBuffer();
	genome = new Genome(genome);

	let rec = JSON.parse(formData.get('meta'));
	rec.asat = Date.now();

	rec = await saveBug(genome, rec);
	// return a success
	return rec;
}


/**
 * if the bug is never going to take any
 * action, there is no reason to keep it.
 * Do a mutation and hope that fixes it, or
 * take a random chance that we skip out
 * (to avoid an infinite loop)
 */
async function FixAct(bug){
	let i = 0;
	let willact = false;
	console.log(`\tBrain Activity Mutation`);
	for(; !willact && i < 1000; i++){
		bug.genome.mutate();
		bug.init();
		willact = WillAct(bug);
		//if(i%9 === 0){
		//	process.stdout.write(`\tBrain Activity Mutation: ${i}\r`);
		//}
	}
	if(!willact){
		ForceAct(bug);
	}
	return i;
}

/**
 * after all of that, it still isn't acting. We are going to
 * take extreme measures: cut it open, and manually rewire it
 * to act.
 */
function ForceAct(bug){
	let min = bug.actuators.byteOffset;
	let rng = bug.actuators.length-1;
	for(let gate of bug.brain.gates){
		for (let o in gate.outputs) {
			// take a small chance there will be a change
			let shouldChange = Math.random();
			shouldChange *= bug.brain.gates.length;
			shouldChange = !Math.floor(shouldChange);
			if(shouldChange){
				// forcefully set the output to an action
				let output  = Math.random();
				output *= rng;
				output += min;
				output = Math.floor(output);
				gate.outputs[o] = output;
			}
		}
	}
	// if this thing had karma, it doesn't anymore
	bug.genome.meta.karma = 0;
}

function WillAct(bug){
	let min = bug.actuators.byteOffset;
	let max = min + bug.actuators.length-1;
	for(let gate of bug.brain.gates){
		for (let output of gate.outputs) {
			if(output >= min && output <= max){
				return true;
			}
		}
	}
	return false;
}
