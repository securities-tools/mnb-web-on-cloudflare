"use strict";

const IBM = {
	someHost: "https://3a6f8a86-396c-43b0-a010-a2d951cbc30c-bluemix.cloudantnosqldb.appdomain.cloud/facts",
	user: 'apikey-v2-15i49yhsabtanithar4ub5lxg2m5m1l78ehqypo037nw',
	pass: '4b06a411de94f9187e343285a9619717',
};
IBM.url = IBM.someHost + "/_design/score/_view/score?reduce=true&group=true",
IBM.token = btoa(`${IBM.user}:${IBM.pass}`);
IBM.fetchinit = {
	headers: {
		"content-type": "application/json;charset=UTF-8",
		"Authorization": `Basic ${IBM.token}`,
	}
};

const ESTIMATEFLAG = 0x80000000;
const MAX_VALUE =    0xffffffff ^ ESTIMATEFLAG;
const ONEDAY = 24 * 60 * 60 * 1000;



function hasher(buffer,bits=32){
	if(typeof buffer === 'string'){
		buffer = buffer
			.split('')
			.map(d=>{return d.charCodeAt(0);})
			;
	}
	if(typeof buffer[Symbol.iterator] !== 'function'){
		console.debug(`buffer: ${buffer}`);
	}
	let size = Math.pow(2,bits);
	let hash = 0;
	for(let d of buffer){
		hash *= 2;
		hash += Math.floor(hash/size);
		hash %= size;
		hash ^= d;
	}
	return hash;
};

let headers =  {
	'Content-Type': 'text/json; charset=utf-8',
	'Access-Control-Allow-Headers': '*',
	'Access-Control-Allow-Methods': 'GET, POST, PUT, OPTIONS',
	'Access-Control-Allow-Origin': '*',
};



function PrepareUpdateRecords(updRecs){
	updRecs = updRecs.map((d)=>{
		let date = Math.round(Date.parse(d.end) / ONEDAY);

		//console.debug(`Prepare: ${d}`);
		let val = d.val;
		//console.log(val);
		if(typeof val !== 'number'){
			//console.log(typeof val);
			val = hasher(val.toString(), BigUint64Array.BYTES_PER_ELEMENT*8);
		}
		val = Number.parseInt(val);
		//console.log(val);

		let fld = d.fact;
		//console.log(fld);
		fld = d._id.split('/')[2]
		//console.log(fld);
		fld = Number.parseInt(fld,36);
		//console.log(fld);

		return {date:date,fld:fld,val:val};
	});
	updRecs = updRecs.sort((a,b)=>{
		return b.date-a.date;
	});


	console.debug('');
	console.debug('# Prepare Updates');
	console.debug(`PrepareUpdates > updRecs: ${JSON.stringify(updRecs)}`);

	return updRecs;
}


function ApplyUpdatesToChunk(params,record,updates){
	// for each field in the record
	for(let i in record){
		// check to see if it is an estimated value
		if(record[i] & ESTIMATEFLAG){
			// if it was a projected value, we should overwrite it with the current projection
			// projections are just ... whatever the most recent value was
			// we should also make sure we point out this is a projection we are making
			record[i] = params.prevRec[i] | ESTIMATEFLAG;
		}
	}

	//console.debug(`- Apply ${record.length}@${record[0]} (${record.at(-1)}) `);
	// going through our update lists, check to see if our last element is associated with the curren't record's date
	while(updates.length > 0 && updates.at(-1).date === record[0]){
		let update = updates.pop();
		let fld = update.fld;
		update.col = params.newFieldMap[fld];
		record[update.col] = update.val;
		/*
		console.debug(`  - ${JSON.stringify(update)} `);
		console.debug(`  - ${fld} `);
		console.debug(`  - ${JSON.stringify(params.newFieldMap)} `);
		console.debug(`  - ${update.col} `);
		console.debug(`  - ${record[update.col]} `);
		*/
	}

	return record;
}

function DataSizeReader(chunk,params) {
	params.processor = CreateHeaderField;

	let sizes = new BigUint64Array(chunk.buffer,0,4);

	console.debug(``);
	console.debug(`# Meta`);
	console.debug(`Company.....: ${sizes[0]}`);
	console.debug(`oldWidth....: ${sizes[1]}`);
	console.debug(`oldFirstDay.: 0x${sizes[2].toString(16)}`);
	console.debug(`oldLastDay..: 0x${sizes[3].toString(16)}`);
	console.debug(``);

	params.company = sizes[0];
	params.oldWidth = sizes[1];
	params.oldFirstDay = sizes[2];
	params.oldLastDay = sizes[3];

	params.nullfld = params.oldWidth-1;
	params.buffRecByteLen = params.oldWidth * 4;

	return [];
}


function CreateHeaderField(chunk,params){
	params.processor = CreateBody;
	params.oldFields = Array.from(chunk);

	params.newFields = Array.from(params.newFields);
	params.newFields = params.newFields.concat(Array.from(params.updFields));
	params.newFields = params.newFields.concat(Array.from(params.oldFields));
	params.newFields = DataBlock.SanitizeFields(params.newFields);
	params.newWidth = params.newFields.length;
	params.blankRec = params.newFields.slice(0);
	params.blankRec = params.blankRec.fill(0+ESTIMATEFLAG);
	params.prevRec = params.blankRec.slice(0);
	params.fieldMap = params.blankRec.slice(0);

	params.newFirstDay = Math.min(params.oldFirstDay,params.updDateMin);
	params.newLastDay = Math.max(params.oldLastDay,params.updDateMax);

	console.debug(``);
	console.debug(`# Header`);
	console.debug(`- Company...: ${params.company}`);
	console.debug(`- width.....: ${params.oldWidth} --> ${params.newWidth}`);
	console.debug(`- Fields....: ${params.oldFields.length} --> ${params.newFields.length}`);
	console.debug(`- FirstDay..: ${params.oldFirstDay} --> 0x${params.newFirstDay.toString(16)}`);
	console.debug(`- LastDay...: ${params.oldLastDay} --> 0x${params.newLastDay.toString(16)}`);
	console.debug(`- oldFields.: ${params.oldFields.slice(0,3)} ... ${params.oldFields.slice(-3)}}`);
	console.debug(`- newFields.: ${params.newFields.slice(0,3)} ... ${params.newFields.slice(-3)}}`);
	console.debug(``);

	if(params.oldFields.length != params.oldWidth || params.oldFields.at(-1) !== 0 || params.oldFields.at(0) !== 1){
		//TODO: Dangerous to ignore
		throw new Error('Issue with field list in old Dataset. Retrieved '+params.oldFields.length+' elements and found value '+params.oldFields.at(-1).toString(16));
	}

	params.fieldMap[0] = 0;
	for(let i=1, m=1; m<params.newWidth; m++){
		if(params.oldFields[i] === params.newFields[m]){
			params.fieldMap[m] = i;
			i++;
		}
		else{
			params.fieldMap[m] = params.nullfld;
		}
	}
	params.newFieldMap = params.newFields.reduce((a,d,i)=>{
		a[d] = i;
		return a;
	},{});

	if(params.newFields.length != params.newWidth || params.newFields.at(-1) !== 0){
		//TODO: Dangerous to ignore
		throw new Error('Issue with field list in new Dataset. Retrieved '+params.newFields.length+' elements and found value '+params.newFields.at(-1).toString(16));
	}

	// encode the new
	try{
		let result = [params.company, params.newWidth, params.newFirstDay, params.newLastDay]
		return [
			result,
			Array.from(params.newFields)
		];
	}
	catch(e){
		console.error(e);
		console.error(e.stack);
		console.error(newFields);
	}

}


function CreateBody(chunk,params){
	let oldRec = chunk;
	let results = [];
	// this could be the one record we get from the initialization routine.
	// If it is, we should initialize the date from the old record
	if(oldRec[0] === 0){
		//console.debug(`Transform > Initialize new record`);
		oldRec[0] = params.newFirstDay;
	}
	let oldDate = oldRec[0];

	//console.debug(`Transform > New Records Front: ${params.updDateMin} --> ${oldDate} ... ${oldDate-params.updDateMin}`);
	for(let day=params.updDateMin; day < oldDate; day++ ){
		let newRec = params.blankRec.slice(0);
		newRec[0] = day;
		newRec = ApplyUpdatesToChunk(params,newRec,params.updRecs);
		results.push(newRec);
	}

	// Make sure the old record is mapped to teh new record shape
	let newRec = params.blankRec.slice(0);
	for(let i=0; i<newRec.length; i++){
		newRec[i] = oldRec[params.fieldMap[i]];
	}
	newRec = ApplyUpdatesToChunk(params,newRec,params.updRecs);
	results.push(newRec);

	//console.debug(`Transform > New Records Back.: ${oldDate+1} --> ${params.updDateMax} ... ${params.updDateMin-(oldDate+1)}`);
	for(let day=oldDate+1; day <= params.updDateMax; day++ ){
		let newRec = params.blankRec.slice(0);
		newRec[0] = day;
		newRec = ApplyUpdatesToChunk(params,newRec,params.updRecs);
		results.push(newRec);
	}
	//console.debug(`Transform > Done.`);
	return results;
}



async function CalcStreamLength(updFields, updRecs) {
	let params = {
		oldWidth: null,
		oldFirstDay: null,
		oldLastDay: null,
		oldFields: null,

		newWidth: null,
		newFirstDay: null,
		newLastDay: null,
		newFields: updRecs.map(d=>{return d.fld;}),

		updDateMax: updRecs.at(0).date,
		updDateMin: updRecs.at(-1).date,
		nullfld: null,
		updRecs: updRecs,
		updFields: updFields,

		fieldMap: null,
		blankRec: null,
		prevRec: null,

		buffChunk: new Uint8Array(0),
		buffPos: 0,
		buffRecByteLen: 16,

		processor: DataSizeReader,
	};


	let counter = 0;
	return new TransformStream(
		{
			async transform(inboundBuffer, controller) {
				try{
					inboundBuffer = new Uint8Array(inboundBuffer.buffer);

					params.buffChunk = params.buffChunk.slice(params.buffPos);
					params.buffPos = 0;

					let newBuff = new Uint8Array(inboundBuffer.length+params.buffChunk.length);
					newBuff.set(params.buffChunk);
					newBuff.set(inboundBuffer,params.buffChunk.length);

					params.buffChunk = newBuff;
				}
				catch(e){
					console.debug(e);
					console.debug(e.stack);
					console.debug(`Buffer: ${params.buffChunk.buffer}`);
					console.debug(`Size: ${params.buffChunk.buffer.byteLength}`);
					throw e;
				}

				while(params.buffChunk.length-params.buffPos >= params.buffRecByteLen){
					counter++;
					let chunk = new BigUint64Array(params.buffChunk.buffer, params.buffPos, params.buffRecByteLen/4);
					params.buffPos += params.buffRecByteLen;
					console.debug(``);
					console.debug(`CalcStreamLength > ChunkSize...: ${params.buffChunk.length}`);
					console.debug(`CalcStreamLength > BytePos.....: ${params.buffPos}`);
					console.debug(`CalcStreamLength > RecordBytes.: ${params.buffRecByteLen}`);
					console.debug(`CalcStreamLength > Counter.....: ${counter}`);
					console.debug(Array.from(chunk).map(d=>d.toString(16)));


					// each processor knows when to change to the next processor
					// and handles that internally
					let results = await params.processor(chunk, params);
					if(counter === 2){

						// 1 header row
						let totalOutLen = 1;
						// however many days
						totalOutLen += params.newLastDay - params.newFirstDay + 1;
						// multiplied by width
						totalOutLen *= params.newWidth
						// meta data fields
						totalOutLen += 4;
						// word size
						totalOutLen *= BigUint64Array.BYTES_PER_ELEMENT;
						console.debug(`Partition Size.: ${totalOutLen}`);

						controller.enqueue(totalOutLen);

						controller.terminate();
						return;
					}

					console.debug('');

				}
			}

		}
	);
}

function DataShapeStreamer(updFields, updRecs) {
	let params = {
		oldWidth: null,
		oldFirstDay: null,
		oldLastDay: null,
		oldFields: null,

		newWidth: null,
		newFirstDay: null,
		newLastDay: null,
		newFields: updRecs.map(d=>{return d.fld;}),

		updDateMax: updRecs.at(0).date,
		updDateMin: updRecs.at(-1).date,
		nullfld: null,
		updRecs: updRecs,
		updFields: updFields,

		fieldMap: null,
		blankRec: null,
		prevRec: null,

		buffChunk: new Uint8Array(0),
		buffPos: 0,
		buffRecByteLen: 16,

		processor: DataSizeReader,
	};


	let counter = 0;
	return new TransformStream(
		{
			async transform(inboundBuffer, controller) {
				if (inboundBuffer === null) {
					controller.terminate();
					if(params.buffChunk.length != params.buffPos){
						throw new Error('Data left in buffer at end of processing');
					}
					return;
				}



				try{
					console.debug(`DataShapeStreamer > inbound...: ${inboundBuffer.length} - ${inboundBuffer.slice(0,3)} ... ${inboundBuffer.slice(-3)}`);
					inboundBuffer = new Uint8Array(inboundBuffer.buffer);

					params.buffChunk = params.buffChunk.slice(params.buffPos);
					params.buffPos = 0;

					let newBuff = new Uint8Array(inboundBuffer.length+params.buffChunk.length);
					newBuff.set(params.buffChunk);
					newBuff.set(inboundBuffer,params.buffChunk.length);

					params.buffChunk = newBuff;
				}
				catch(e){
					console.debug(e);
					console.debug(e.stack);
					console.debug(`Buffer: ${params.buffChunk.buffer}`);
					console.debug(`Size: ${params.buffChunk.buffer.byteLength}`);
					throw e;
				}

				while(params.buffChunk.length-params.buffPos >= params.buffRecByteLen){
					let chunk = new BigUint64Array(params.buffChunk.buffer, params.buffPos, params.buffRecByteLen/4);
					params.buffPos += params.buffRecByteLen;

					console.debug(`DataShapeStreamer > ChunkSize...: ${params.buffChunk.length}`);
					console.debug(`DataShapeStreamer > BytePos.....: ${params.buffPos}`);
					console.debug(`DataShapeStreamer > RecordBytes.: ${params.buffRecByteLen}`);
					console.debug(`DataShapeStreamer > Counter.....: ${counter++}`);
					console.debug(Array.from(chunk).map(d=>d.toString(16)));
					// each processor knows when to change to the next processor
					// and handles that internally
					let results = params.processor(chunk, params);

					for(let result of results){
						result = new BigUint64Array(result);
						console.debug(Array.from(result).map(d=>d.toString(16)));
						result = new Uint8Array(result.buffer);
						controller.enqueue(result);

						/*
						console.debug('o|'+Array.from(result)
							.map(d=>{ return d.toString(16).padStart(8,'0');})
							.join('.')
							.substr(0,80)
						);
						*/

					}
					//console.debug('');

				}
			}

		}
	);
}

class DataBlock extends BigUint64Array{
	constructor(fields, firstday, lastday){
		if (fields instanceof BigUint64Array) {
			super(fields);
			return;
		}

		fields = DataBlock.SanitizeFields(fields);
		if (firstday > lastday) {
			let tmp = firstday;
			firstday = lastday;
			lastday = tmp;
		}

		let X = fields.length;
		let Y = Math.max(1,Math.round((lastday - firstday)/ONEDAY)+1);

		super(2+X+(X*Y));
		this.fill(0);
		this[0] = X;
		this[1] = Y;
		this.set(fields,2);

		// populate the date field in all
		let day = new Date(firstday);
		day = day.toISOString().split('T')[0].split('-');
		day = new Date(Date.UTC(day[0],day[1]-1,day[2]));
		for(let d=X+2; d<this.length; d+=X ){
			this[d] = Math.round(day.getTime()/this.ONEDAY);
			day.setDate(day.getDate()+1);
		}

		this._ = {};
	}

	get ONEDAY(){
		let d = 24 * 60 * 60 * 1000;
		Object.defineProperty(this,'ONEDAY',{get:()=>{return d;}});
		return d;
	}

	get MAX_SAFE_INTEGER(){
		let d = 2**(this.BYTES_PER_ELEMENT*8)-1;
		Object.defineProperty(this,'MAX_SAFE_INTEGER',{get:()=>{return d;}});
		return d;
	}

	get width(){
		return this[0];
	}

	get height(){
		return this[1];
	}

	get firstday(){
		let d = this.width;
		d = this[2+d];
		//console.log(d);
		d *= this.ONEDAY;
		//console.log(d);
		d = new Date(d);
		//console.log(d);
		Object.defineProperty(this,'firstday',{get:()=>{return d;}});
		return d;
	}

	get lastday(){
		let d = this[this.length-this.width];
		d *= this.ONEDAY;
		d = new Date(d);
		Object.defineProperty(this,'lastday',{get:()=>{return d;}});
		return d;
	}

	get body(){
		let b = new BigUint64Array(this.buffer,(this.width+2)*BigUint64Array.BYTES_PER_ELEMENT);
		Object.defineProperty(this,'body',{get:()=>{return b;}});
		return b;
	}

	get fields(){
		let d = new BigUint64Array(this.buffer, 2*BigUint64Array.BYTES_PER_ELEMENT, this.width);
		Object.defineProperty(this,'fields',{get:()=>{return d;}});
		return d;
	}

	get fieldmap(){
		let d = this.fields.reduce((a,d,i)=>{
			a[d] = i;
			return a;
		},{});
		Object.defineProperty(this,'fieldmap',{get:()=>{return d;}});
		return d;
	}


	DateToRow(date){
		let row = (date - this.firstday) / this.ONEDAY;
		return row;
	}


	SanitizeFields(fields){
		return DataBlock.SanitizeFields(fields);
	}


	Widen(newFields){
		newFields = newFields.concat(this.fields);
		newFields = this.SanitizeFields(newFields);
		if(this.fields.length === newFields.length){
			return this;
		}

		let curFlds = this.fields;
		let newWidth = newFields.length;
		let nullfld = newWidth-1;
		let map = new Array(newWidth);
		map[0] = 0;
		for(let i=1, m=0; m<newWidth; m++){
			if(curFlds[i] === newFields[m]){
				map[m] = i;
				i++;
			}
			else{
				map[m] = nullfld;
			}
		}
		let newdoc = new DataBlock(newFields,this.firstday,this.lastday);

		for(let i=2, y=0; y<this.height; y++){
			for(let x=0; x<newdoc.width; x++, i++){
				newdoc[i] = this[2+y+map[x]];
			}
		}

		return newdoc;
	}


	ExpandFront(firstday){
		let days = this.firstday - firstday;
		if(days < 1) return this;
		days = Math.round(days/this.ONEDAY);
		let length = days * this.width;
		let inject = 0;
		try{
			inject = new BigUint64Array(length);
			inject = inject.fill(0);
		}
		catch(e){
			console.log(`Days.....: ${days}`);
			console.log(`Width....: ${this.width}`);
			console.log(`Length...: ${length}`);
			console.log(`CurFirst.: ${this.firstday}`);
			console.log(`NewFirst.: ${firstday}`);
		}

		// populate the date field in all
		for(let day=new Date(this.lastday), d=0; d < days; day.setDate(day.getDate()+1), d++ ){
			inject[2 + this.width * d] = day.getTime();
		}

		let newdata = new BigUint64Array(this.length + inject.length);
		newdata[0] = this[0];
		newdata[1] = this[1] + inject.length;
		newdata.set(this, 0, 2+this.width);
		newdata.set(inject, 2+this.width, inject.length);
		newdata.set(this.body, 2+this.width+inject.length);

		let newdoc = new DataBlock(newdata);
		return newdoc;
	}


	ExpandBack(lastday){
		let days = lastday - this.lastday;
		if(days < 1) return this;
		days = Math.round(days/this.ONEDAY);

		let length = this.length + days * this.width;

		let newdata = null;
		try{
			newdata = new BigUint64Array(length);
			newdata = newdata.fill(0);
			newdata.set(this, 0);
		}
		catch(e){
			console.log(`Days.....: ${days}`);
			console.log(`Width....: ${this.width}`);
			console.log(`Length...: ${length}`);
			console.log(`CurFirst.: ${this.lastday}`);
			console.log(`NewFirst.: ${lastday}`);
			console.log(e);
			console.log(e.stack);
		}

		// populate the date field in all
		for(let day=new Date(this.lastday), d=2+this.width; d < days; day.setDate(day.getDate()+1), d+=this.width ){
			newdata[d] = day.getTime();
		}

		let newdoc = new DataBlock(newdata);
		return newdoc;
	}


	IntegrateData(changes){
		for(let change of changes){
			//console.log(change);
			let val = change.val;
			//console.log(val);
			if(typeof val !== 'number'){
				//console.log(typeof val);
				val = hasher(val.toString(), BigUint64Array.BYTES_PER_ELEMENT*8);
			}
			val = Number.parseInt(val);
			//console.log(val);

			let fld = change.fact;
			//console.log(fld);
			fld = change._id.split('/')[2]
			//console.log(fld);
			fld = Number.parseInt(fld,36);
			//console.log(fld);
			let col = this.fieldmap[fld];
			//console.log(col);

			let date = Date.parse(change.end);
			//console.log(date);
			let row = this.DateToRow(date);
			//console.log(row);

			//console.log(`body[${row},${col}] = ${val}`);
			this.body[row*col] = val;
		}
		return this;
	}
}

DataBlock.SanitizeFields = function(fields){
	if (!Array.isArray(fields)) {
		console.log(fields);
		throw Error('Must be an array of fields');
	}
	fields = new Set(fields);
	fields = Array.from(fields);
	fields = fields
		.filter(d=>{
			return (typeof d === 'number');
		})
		.filter(d=>{
			return (d!==0 && d!==1);
		})
		.sort((a,b)=>{
			return a - b;
		})
		;
	fields = [1].concat(fields,[0]);
	fields = new BigUint64Array(fields);
	return fields;
}


async function ParseRequest(context){
	let formData = await context.request.formData();

	let data = JSON.parse(formData.get('value'));
	let type = formData.get('type');
	let key = formData.get('key');

	key = [type,key].join('/');

	let rtn = {key: key, type: type, doc: data};
	return rtn;
}


function SanitizeValues(doc){
	for(let fld in doc){
		if(fld.startsWith('_')){
			delete doc[fld];
		}
	}
	return doc;
}


async function IntegrateView(context, req){
	let doc = await context.env.facts.get(req.key);
	doc = await doc.json();

	req.doc = Object.assign(doc,req.doc);
	return req;
}


async function GetStream(facts, key){
	console.log(`---`);

	// Get the existing document from the database
	let stm = null;
	let rev = 0
	let docs = await facts.list({prefix:key});
	if(docs.objects.length > 0){
		console.log(`Found: ${key}`);
		rev = docs.objects
			.sort((a,b)=>{
				let diff = b.key.localeCompare(a.key);
				diff = diff || b.uploaded.getTime() - a.uploaded.getTime();
				diff = diff || b.version.localeCompare(a.version);
				return diff;
			})
			.pop()
			.key
			.split('/')
			.pop()
			;
		rev = Number.parseInt(rev);
		let doc = await facts.get(`${key}/${rev}`);
		stm = doc.body;
	}
	if(!stm){
		console.log(`Not Found: ${key}`);
		let comp = key.split('/').pop().split('').reverse().join('');
		rev = 0;
		stm = new ReadableStream({
			start(controller) {
				let v = new BigUint64Array([
					//meta data
					comp, 2, MAX_VALUE, 0,
					//header row
					1, 0,
					//body row
					0, 0,
				]);
				controller.enqueue(v);
				controller.close();
			}
		});
	}
	let rtn = {
		key: key,
		rev: rev,
		stm: stm
	}
	return rtn;

}


async function IntegrateCompany(context, req){
	// How many fields should we have?
	let fields = await context.env.facts.get('views/facts');
	if (fields) {
		fields = await fields.json();
	}
	else {
		let url = `${IBM.someHost}/labels%2Ffacts`;
		console.log(`FETCH: ${url}`);
		fields = await fetch(url, IBM.fetchinit);
		fields = await fields.json();
		await context.env.facts.put('views/facts',JSON.stringify(fields));
	}
	fields = Array.from(Object.values(fields));

	req.doc = PrepareUpdateRecords(req.doc);

	let transform = await CalcStreamLength(fields, req.doc);
	let limiter = await GetStream(context.env.facts, req.key);
	req.rev = limiter.rev;
	limiter = limiter.stm.pipeThrough(transform);
	let reader = limiter.getReader();
	let totalOutLen = await reader.read();
	totalOutLen = totalOutLen.value;
	console.debug('FixedLen: '+ JSON.stringify(totalOutLen));
	limiter = new FixedLengthStream(totalOutLen);

	let shape = await GetStream(context.env.facts, req.key);
	req.doc = shape.stm
		.pipeThrough(DataShapeStreamer(fields, req.doc))
		.pipeThrough(limiter)
		;

	return req;
}


export async function onRequestPost(context) {
	try{
		let req = await ParseRequest(context);
		console.log('REQ:'+JSON.stringify(req));
		console.log(req);

		req.doc = await SanitizeValues(req.doc);

		let integrator = null;
		switch(req.type){
			case 'view'   : integrator = IntegrateView   ; break;
			case 'company': integrator = IntegrateCompany; break;
			case '': break;
			default:
				throw new Error('Unrecognized type: ' + req.type);
		}

		let record = {};
		if(integrator){
			let key = req.key;
			let rem = async ()=>{};
			if(req.type === 'company'){
				let oldKey = `${req.key}/${req.rev}`;
				let rev = req.rev + 1;
				key = `${req.key}/${rev}`;
				rem = async ()=>{
					await context.env.facts.delete(oldKey);
				}
			}
			req = await integrator(context, req);
			record = await context.env.facts.put(key,req.doc);
			await rem();
		}
		return new Response(JSON.stringify(record), {
			headers:headers,
			status: 200
		});
	}
	catch(e){
		console.error(e);
		console.error(e.stack);
		return new Response(e.toString(), {
			headers:headers,
			status: 500
		});
	}
}


export async function onRequestPut(context) {
	return onRequestPost(context);
}
