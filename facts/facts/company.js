import * as utils from '../../public/lib/utils.js';

let headers =  {
	'Content-Type': 'application/octet',
	'Access-Control-Allow-Headers': '*',
	'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
	'Access-Control-Allow-Origin': '*',
};


function CreateFilter(date, days){
	let params = {
		state: 'meta',
		latest: utils.DateToDay(utils.utcParse(date)),
		buffer: new Uint8Array(),
		reclen: 4,
		meta: null,
		seek: null,
		send: days,
	};
	params.earliest = params.latest - days;

	return new TransformStream({
		async transform(inboundBuffer, controller) {
			if(params.state === 'end'){
				controller.terminate();
				return;
			}
			if (inboundBuffer === null) {
				controller.terminate();
				return;
			}
			let partA = new Uint8Array(buffer);
			let partB = new Uint8Array(inboundBuffer.buffer);

			let newBuff = new Uint8Array(partA.length+partB.length);
			newBuff.set(partA);
			newBuff.set(partB, partA.length);

			//console.debug(`DataShapeStreamer > newBuff...: ${new BigUint64Array(newBuff.buffer)}`);

			let buffPos = 0;
			while( (newBuff.length-buffPos)/4 >= params.reclen){
				let chunk = new BigUint64Array(newBuff.buffer, buffPos, params.reclen);
				buffPos += params.reclen;

				switch(params.state){
					case 'meta':
						params.state = 'header';
						params.meta = Array.from(chunk);
						params.reclen = params.meta[1];
						params.seek = Math.max(0, params.meta[2]-params.latest);
						this.push(new Uint8Array([
							params.meta[0], // the company
							params.reclen,
							params.latest,
							params.earliest,
						]));
						break;
					case 'header':
						params.state = 'send';
						controller.enqueue(chunk);
						break;
					case 'send':
						params.seek--;
						if(params.seek <= 0 && params.send > 0){
							controller.enqueue(chunk);
							params.send--;
						}
						else if(params.send <= 0){
							params.state = 'end';
						}
						break;
				}
			}
			params.buffChunk = newBuff.slice(buffPos*4)
			return;
		}
	});
}

async function getCompanyFacts(env,comp,date,days){
	days = days ?? 3650/2;
	date = date ?? (new Date()).toISOString().split('T').shift();
	if(!comp){
		headers['Content-Type'] = 'text/plain; charset=utf-8';
		return new Response('No Company provided', {
			headers:headers,
			status: 400
		});
	}

	try{
		let key = `company/${comp}`;
		console.log(`Fetching '${key}'`);

		let facts = await env.facts.get(key);
		console.debug(facts);
		if(!facts){
			headers['Content-Type'] = 'text/plain; charset=utf-8';
			return new Response(`Company (${comp}) not found (key: ${key})`, {
				headers:headers,
				status: 404
			});
		}
		console.log(`Found: ${key}`);

		headers['Content-Disposition'] = `attachment; filename="C-${comp}.facts"`;
		headers['Content-Encoding'] = 'br';

		let stm = facts.body
			//.pipeThrough(new DecompressionStream("brotli"))
			//.pipeThrough(CreateFilter(date, days))
			//.pipeThrough(new CompressionStream("brotli"))
			;

		return new Response(stm, {
			headers:headers,
			status: 200
		});
	}
	catch(err){
		console.error(err);
		headers['Content-Type'] = 'text/plain; charset=utf-8';
        return new Response(err.message, {
            headers:headers,
            status: 500
        });
	}
}


export async function onRequestGet(request, env, context) {
	console.log('company - onRequestGet');
	console.log(request.url);
	let url = new URL(request.url);
	let formData = new URLSearchParams(url.search);

	let comp = formData.get('cik');
	let date = formData.get('date');
	let days = formData.get('days');

	let facts = getCompanyFacts(env,comp,date,days);
	return facts;
}
