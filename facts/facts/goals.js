/**
 * Retrieves a goal for our animats to research
 *
 * Goals consiste of a company and a date for which to suggest values
 */

let headers =  {
	'Content-Type': 'text/json; charset=utf-8',
	'Access-Control-Allow-Headers': '*',
	'Access-Control-Allow-Methods': 'GET',
	'Access-Control-Allow-Origin': '*',
};


const RANGE = Math.floor(364.25*5);

export async function onRequestGet(request, contextEnv, context)  {
	let purpose = {
		'comp': null,
		'date': null,
		'days': RANGE,
	};

	// Get the list of companies
	let allcomps = await contextEnv.facts.get('views/symbols');
	allcomps = await allcomps.json();
	allcomps = Object.values(allcomps).map(d=>d.cik).filter(d=>d);

	let min = 0;
	let max = 0;
	while(max-min <= RANGE && allcomps.length > 0){
		// pick one at random
		let rand = Math.floor(Math.random()*allcomps.length);
		purpose.comp = allcomps[rand];
		// preserve the last item in case we have to do this again
		allcomps[rand] = allcomps.pop();
		//purpose.comp = 1075124;

		//Look up its processing date range
		let dates = await contextEnv.facts.head(`company/${purpose.comp}`);
		dates = dates.customMetadata;
		delete dates.acquireindex;
		dates = Object.values(dates).map(d=>JSON.parse(d));
		dates = [].concat(...dates);
		// find possible dates that we can use for analysis
		// we require at least 5 years (RANGE) of data
		max = Math.max(...dates);
		min = Math.min(...dates);
	}
	purpose.date = Math.round((max-min-RANGE)*Math.random() + min + RANGE);

	purpose = JSON.stringify(purpose);
	return new Response(purpose, {
		headers:headers,
		status: 200
	});

}
