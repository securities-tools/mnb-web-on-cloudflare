

export async function onRequestGet(context) {
	try {
		let results = await ScanGuf(context.env);
		let str = JSON.stringify(results);

		return new Response(str, {
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				'Access-Control-Allow-Headers': '*',
				'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
				'Access-Control-Allow-Origin': '*',
			},
			status: 200
		});
	}
	catch (err) {
		return new Response('Error parsing JSON content', { status: 400 });
	}
}


async function ScanGuf(env){
	let scores = {};
	//console.log(`ScanGuf: start`);

	let genomes = await env.EdgarBrains.list({prefix:'guf/',include: ['customMetadata']});
	//console.log(`ScanGuf: genomes length is ${genomes.objects.length}`);
	while( genomes.objects.length > 0){
		for(let genome of Array.from(genomes.objects)){
			//console.log(`ScanGuf: bug ${genome.customMetadata.meta}`);
			let meta = JSON.parse(genome.customMetadata.meta);
			let id = meta.genome.id;
			id = genome.key.split('/').pop();
			let score = meta.bug.score;
			//console.log(`ScanGuf: ${id} - ${score}`);
			meta.genome.score = score;
			meta.genome.id = id;
			meta.genome.ova = meta.bug.ova;
			scores[id] = meta.genome;
		}
		genomes = genomes.truncated ? env.EdgarBrains.list({cursor:genomes.cursor}) : {objects:[]};
		genomes = await genomes;
		//console.log(`ScanGuf: genomes length is ${genomes.objects.length}`);
	}

	//console.log(`ScanGuf: scores ${JSON.stringify(scores)}`);
	return scores;
}
