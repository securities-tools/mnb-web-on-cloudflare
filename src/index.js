import { getAssetFromKV, NotFoundError, MethodNotAllowedError } from '@cloudflare/kv-asset-handler'
import { Router } from 'itty-router';

import manifestJSON from '__STATIC_CONTENT_MANIFEST'
const assetManifest = JSON.parse(manifestJSON)

import * as api from '../facts/index.js';

export default {
	async fetch(request, env, ctx) {
		console.log(`Start`);
		let rtn = null;

		rtn = rtn || await GetStaticPage(request, env, ctx);
		rtn = rtn || await GetAPIPage(request, env, ctx);
		rtn = rtn || new Response('Cannot find page', { status: 404 })
		return rtn;
	}
}


async function GetAPIPage(request, env, ctx){
	console.log('GetAPIRounter');
	ctx.request = request;
	ctx.env = env;

	// Create a new router
	const router = Router();
	//router.get('/facts/:text', ({ params }) => {});
	router.get('/evolve/breeder', ({ params }) => api.evolveBreeder.onRequestGet(request, env, ctx) );
	router.get('/facts/company', ({ params }) => api.factsCompany.onRequestGet(request, env, ctx) );
	router.get('/facts/goals', ({ params }) => api.factsGoals.onRequestGet(request, env, ctx) );
	router.get('/scores', ({ params }) => api.scores.onRequestGet(ctx) );

	router.put('/evolve/breeder', ({ params }) => api.evolveBreeder.onRequestPut(request, env, ctx) );

	router.all('*', () => new Response('Cannot find api', { status: 404 }));
	return await router.handle(request);
}


async function GetStaticPage(request, env, ctx) {
	let rtn = null;
	console.log(`METHOD: ${request.method}`);
	if(request.method !== 'GET'){
		return null;
	}

	try {
		rtn = await getAssetFromKV(
			{
				request,
				waitUntil(promise) {
					return ctx.waitUntil(promise)
				},
			},
			{
				ASSET_NAMESPACE: env.__STATIC_CONTENT,
				ASSET_MANIFEST: assetManifest,
			},
		);
		console.log(typeof rtn);
	}
	catch (e) {
		if (e instanceof NotFoundError) {
			console.log('NotFound');
		}
		//else if (e instanceof MethodNotAllowedError) {
		//	console.log('Not Allowed');
		//}
		else {
			console.log(e);
			return new Response('An unexpected error occurred', { status: 500 })
		}
	}

	return rtn;
}
